<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - Productos</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
	</head>
	<body id="main">
		<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
		<jsp:include page="/WEB-INF/views/comun/productos.jsp"></jsp:include>
		<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
	</body>
</html>