<%@page import="py.com.carritonline.util.Constantes" %>
<%@page import="py.com.carritonline.bean.Producto" %>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
Producto producto = (Producto)request.getSession().getAttribute(Constantes.PRODUCTO);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - Producto Detalles</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/producto.css" type="text/css" />
		<script type="text/javascript">
			function cantidadProducto() {
				let cantidadProducto = parseInt(document.getElementById("txt-cantidad").value);
				if (cantidadProducto < 1) {
					alert("Debe elegiar una cantidad mayor a cero");
					return false;
				}
				return true;
			}
		</script>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container contenido-estilo" style="padding-bottom: 10px;">
				<%if(producto == null){%>
					<div class="panel panel-info">
					  <div class="panel-heading col-sm-12">
					    <h3 class="panel-title">Detalle Producto</h3>
					  </div>
					  <div class="panel-body col-sm-12">
					    Sin producto
					  </div>
					</div>
				<%}else{%>
					<div class="row">
						<div class="col">
							<h3>Detalle Producto</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<img class="imagen-tamanho" style="background-image: url('<%=request.getContextPath()%>/resources/imgs/<%=producto.getNombreImg()%>.png');">
						</div>
					</div>
					<div class="row" style="text-align: left;">
						<div class="col-md-2 col-lg-2 col-xl-2"></div>
						<div class="col-sm-12 col-md-10 col-lg-10 col-xl-10">
							<b>Descripcion: <%=producto.getDescripcion()%></b>
						</div>
					</div>
					<div class="row" style="text-align: left;">
						<div class="col-md-2 col-lg-2 col-xl-2"></div>
						<div class="col-sm-12 col-md-10 col-lg-10 col-xl-10">
							<b>Precio: </b><%=producto.getPrecioUnit()%>
						</div>
					</div>
					<div class="row" style="text-align: left;">
						<div class="col-md-2 col-lg-2 col-xl-2"></div>
						<div class="col-sm-12 col-md-10 col-lg-10 col-xl-10">
							<b>Cantidad Disponible: </b><%=producto.getCantidad()%>
						</div>
					</div>
					<div class="row" style="text-align: left;">
						<div class="col-md-2 col-lg-2 col-xl-2"></div>
						<div class="col-sm-12 col-md-10 col-lg-10 col-xl-10">
							<form action="<%=request.getContextPath()%><%=WebRequest.PRODUCTOS%>">
								<input type="hidden" value="anhadir" name="accion" />
			                    <b>Cantidad a Comprar:</b>
			                    <input type="hidden" value="<%=producto.getIdProducto()%>" name="idProducto"/>
			                    <input type="number" value="0" id="txt-cantidad" name="cantidad"/>
								<div class="col-sm-10" style="text-align: center; margin-top: 20px;">
									<button type="submit" onclick="return cantidadProducto()">
			                        	Añadir al carrito
			                        </button>
								</div>
							</form>
						</div>
					</div>
				<%}%>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>