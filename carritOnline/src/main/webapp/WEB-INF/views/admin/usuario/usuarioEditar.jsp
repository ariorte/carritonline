<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%@page import="py.com.carritonline.util.Constantes" %>
<%@page import="py.com.carritonline.bean.Usuario" %>
<%
Usuario usuario = (Usuario)request.getSession().getAttribute(Constantes.USUARIO);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - Crear Usuario</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
		<script type="text/javascript">
			function validarIngresado(){
				let nombre = document.getElementById("nombre").value;
				let apellido = document.getElementById("apellido").value;
				let logiName = document.getElementById("logiName").value;
				let contrasena = document.getElementById("contrasena").value;
				let tipoUser = document.getElementById("tipoUser").value;
				
				if (campoUndefined(nombre)
							|| campoUndefined(apellido) 
							|| campoUndefined(logiName)
							|| campoUndefined(contrasena)
							|| campoUndefined(tipoUser)
							|| campoVacio(nombre)
							|| campoVacio(apellido)
							|| campoVacio(logiName)
							|| campoVacio(contrasena)
							|| campoVacio(tipoUser)){
						alert("Campos obligatorios se debe ingresar (*)");
						return false;
					}
				return true;
			}

			function campoUndefined(campo) {
				return (campo == undefined || campo == "undefined" ? true
						: false);
			}

			function campoVacio(campoEvaluar) {
				return (campoEvaluar.trim().length == 0 ? true : false);
			}
		</script>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container padding-not-left-right contenido-estilo" style="padding-bottom: 20px; padding-top: 10px;">
				<form action="<%=request.getContextPath()%><%=WebRequest.USUARIO_EDITADO%>" method="post">
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Nombre*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre" value="<%=usuario.getNombre()%>" required="required"/>
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Apellido*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="apellido" name="apellido" placeholder="Ingrese el apellido" value="<%=usuario.getApellido()%>" required="required" />
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>LoginName*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="logiName" name="logiName" placeholder="Ingrese el LoginName" value="<%=usuario.getLoginName()%>" required="required" />
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Contraseña*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="password" id="contrasena" name="contrasena" placeholder="Ingrese la Contraseña" value="<%=usuario.getContrasenha()%>" required="required" />
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Tipo de Usuario*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="tipoUser" name="tipoUser" placeholder="Ingrese el tipo de usuario" pattern="[0-1]" value="<%=usuario.getTipoUsuario()%>" required="required" />
						</div>
					</div>
					<input type="hidden" name="idUsuario" value="<%=usuario.getIdUsuario()%>" />
					<input type="hidden" name="usuarioCrear" value="crear"/>
					<input type="submit" value="Editar" onclick="return validarIngresado()"/>
				</form>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>