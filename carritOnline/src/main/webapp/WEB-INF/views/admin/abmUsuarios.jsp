<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="py.com.carritonline.util.Constantes"%>
<%@page import="py.com.carritonline.bean.Usuario"%>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%
List<Usuario> usuarios = (List<Usuario>)request.getSession().getAttribute(Constantes.LISTA_COMPLETO_USUARIO);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - AMB Usuarios</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container padding-not-left-right contenido-estilo" style="padding-bottom: 10px;">
				<table class="table">
				  <thead class="thead-light">
				    <tr>
				    	<th scope="col">Fila</th>
				      	<th scope="col">LoginName</th>
				      	<th scope="col">Nombres</th>
				      	<th scope="col">Apellidos</th>
				      	<th scope="col">Tipo de Usuario</th>
				      	<th scope="col">Acciones</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<%
				  	int contar = 1;
				  	for(Usuario usuario : usuarios) {%>
				  		<tr>
					      <th scope="row"><%=contar%></th>
					      <td><%=usuario.getLoginName()%></td>
					      <td><%=usuario.getNombre()%></td>
					      <td><%=usuario.getApellido()%></td>
					      <td><%=usuario.getTipoUsuario()%></td>
					      <td>
					      	<form action="<%=request.getContextPath()%><%=WebRequest.ABM_USUARIOS%>" method="post">
			      				<input type="hidden" name="accionABMUsuario" value="editar"/>
			      				<input type="hidden" name="accionABMUsuarioEdit" value="<%=usuario.getIdUsuario()%>"/>
			      				<button type="submit">Editar</button>
					      	</form>
					      	<form action="<%=request.getContextPath()%><%=WebRequest.ABM_USUARIOS%>" method="post">
					      		<input type="hidden" name="accionABMUsuario" value="eliminar"/>
			      				<input type="hidden" name="accionABMUsuarioEliminar" value="<%=usuario.getIdUsuario()%>"/>
			      				<button type="submit">Eliminar</button>
					      	</form>
					      </td>
				    	</tr>
				  	<%
				  		contar++;
				  	}%>
				  </tbody>
				</table>
				<div class="col-sm-12">
					<form action="<%=request.getContextPath()%><%=WebRequest.ABM_USUARIOS%>" method="post">
						<input type="hidden" name="accionABMUsuario" value="crear"/>
						<button type="submit">Crear Usuarios</button>
					</form>
				</div>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>