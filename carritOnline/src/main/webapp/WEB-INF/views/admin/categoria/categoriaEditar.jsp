<%@page import="py.com.carritonline.util.Constantes" %>
<%@page import="py.com.carritonline.bean.Categoria" %>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
Categoria categoria = (Categoria)request.getSession().getAttribute(Constantes.CATEGORIA);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - Categoria Editar</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
		<script type="text/javascript">
			function validarIngresado(){
				let idCategoria = document.getElementById("idCategoria").value;
				let descripcion = document.getElementById("descripcion").value;
				
				if (campoUndefined(idCategoria)
							|| campoUndefined(descripcion)
							|| campoVacio(idCategoria)
							|| campoVacio(descripcion)){
						alert("Campos obligatorios se debe ingresar (*)");
						return false;
					}
				return true;
			}

			function campoUndefined(campo) {
				return (campo == undefined || campo == "undefined" ? true
						: false);
			}

			function campoVacio(campoEvaluar) {
				return (campoEvaluar.trim().length == 0 ? true : false);
			}
		</script>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container padding-not-left-right contenido-estilo" style="padding-bottom: 20px; padding-top: 10px;">
				<form action="<%=request.getContextPath()%><%=WebRequest.CATEGORIA_EDITADO%>" method="post">
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Id Categoría*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="idCategoria" name="idCategoria" placeholder="Ingrese el id de la categoria" value="<%=categoria.getIdCategoria()%>" required="required"/>
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Descripción*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="descripcion" name="descripcion" placeholder="Ingrese la descripcion de la categoria" value="<%=categoria.getDescripcion()%>" required="required" />
						</div>
					</div>
					
					<input type="hidden" name="categoriaEditar" value="editar"/>
					<input type="submit" value="Editar Categoria" onclick="return validarIngresado()"/>
				</form>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>