<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - Crear Producto</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
		<script type="text/javascript">
			function validarIngresado(){
				let idProducto = document.getElementById("idProducto").value;
				let descripcion = document.getElementById("descripcion").value;
				let cantidad = document.getElementById("cantidad").value;
				let precio = document.getElementById("precio").value;
				let imgsNombre = document.getElementById("imgsNombre").value;
				let idCategoria = document.getElementById("idCategoria").value;

				
				if (campoUndefined(idProducto)
							|| campoUndefined(descripcion)
							|| campoUndefined(cantidad)
							|| campoUndefined(precio)
							|| campoUndefined(imgsNombre)
							|| campoUndefined(idCategoria || campoVacio(idProducto)
									|| campoVacio(descripcion)
									|| campoVacio(cantidad) || campoVacio(precio)
									|| campoVacio(imgsNombre)
									|| campoVacio(idCategoria))){
						alert("Campos obligatorios se debe ingresar (*)");
						return false;
				}
				
				if (parseInt(cantidad) < 1) {
					alert("Debe ingresar una cantidad mayor a cero");
					return false;
				}
				
				if (parseInt(precio) < 1) {
					alert("Debe ingresar un precio mayor a cero");
					return false;
				}
				
				return true;
			}

			function campoUndefined(campo) {
				return (campo == undefined || campo == "undefined" ? true
						: false);
			}

			function campoVacio(campoEvaluar) {
				return (campoEvaluar.trim().length == 0 ? true : false);
			}
		</script>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container padding-not-left-right contenido-estilo" style="padding-bottom: 20px; padding-top: 10px;">
				<form action="<%=request.getContextPath()%><%=WebRequest.PRODUCTO_NUEVO_EDITADO%>" method="post">
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Id producto*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="idProducto" name="idProducto" placeholder="Ingrese el id del producto" required="required"/>
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Descripción*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="descripcion" name="descripcion" placeholder="Ingrese la descripcion del producto" required="required" />
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Cantidad*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="number" id="cantidad" name="cantidad" placeholder="Ingrese la cantidad del producto" required="required" />
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Precio*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="number" id="precio" name="precio" placeholder="Ingrese el precio del producto" required="required" />
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>Nombre de la imagen*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="imgsNombre" name="imgsNombre" placeholder="Ingrese el nombre de la foto del producto" required="required" />
						</div>
					</div>
					<div class="row alinear-texto-izquirda">
						<div class="col-sm-2"></div>
						<div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<label>ID Categoría*:</label>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<input type="text" id="idCategoria" name="idCategoria" placeholder="Ingrese el id de la categoria que va a pertencer el producto" required="required" />
						</div>
					</div>
					<input type="hidden" name="productoCrearEditar" value="crear"/>
					<input type="submit" value="Crear Producto" onclick="return validarIngresado()" style="margin-top: 5px;"/>
				</form>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>