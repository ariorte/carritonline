<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="py.com.carritonline.util.Constantes"%>
<%@page import="py.com.carritonline.bean.Producto"%>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%
List<Producto> listaProductos = (List<Producto>)request.getSession().getAttribute(Constantes.LISTA_COMPLETO_PRODUCTO);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - AMB Productos</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container padding-not-left-right contenido-estilo" style="padding-bottom: 10px;">
				<table class="table">
				  <thead class="thead-light">
				    <tr>
				    	<th scope="col">Fila</th>
				      	<th scope="col">ID Producto</th>
				      	<th scope="col">Descripción</th>
				      	<th scope="col">Cantidad Disponible</th>
				      	<th scope="col">Precio x Unidad</th>
				      	<th scope="col">Acciones</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<%
				  	int contar = 1;
				  	for(Producto producto : listaProductos) {%>
				  		<tr>
					      <th scope="row"><%=contar%></th>
					      <td><%=producto.getIdProducto()%></td>
					      <td><%=producto.getDescripcion()%></td>
					      <td><%=producto.getCantidad()%></td>
					      <td><%=producto.getPrecioUnit()%></td>
					      <td>
					      	<form action="<%=request.getContextPath()%><%=WebRequest.ABM_PRODUCTOS%>" method="post">
			      				<input type="hidden" name="accionAMBProducto" value="editar"/>
			      				<input type="hidden" name="accionAMBProductoEdit" value="<%=producto.getIdProducto()%>"/>
			      				<button type="submit">Editar</button>
					      	</form>
					      	<form action="<%=request.getContextPath()%><%=WebRequest.ABM_PRODUCTOS%>" method="post">
					      		<input type="hidden" name="accionAMBProducto" value="eliminar"/>
			      				<input type="hidden" name="accionAMBProductoEliminar" value="<%=producto.getIdProducto()%>"/>
			      				<button type="submit">Eliminar</button>
					      	</form>
					      </td>
				    	</tr>
				  	<%
				  		contar++;
				  	}%>
				  </tbody>
				</table>
				<div class="col-sm-12">
					<form action="<%=request.getContextPath()%><%=WebRequest.ABM_PRODUCTOS%>" method="post">
						<input type="hidden" name="accionAMBProducto" value="crear"/>
						<button type="submit">Crear Producto</button>
					</form>
				</div>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>