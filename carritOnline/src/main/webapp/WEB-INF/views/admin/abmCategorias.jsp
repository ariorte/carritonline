<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="py.com.carritonline.util.Constantes"%>
<%@page import="py.com.carritonline.bean.Categoria"%>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%
List<Categoria> listaCategoria = (List<Categoria>)request.getSession().getAttribute(Constantes.LISTA_COMPLETO_CATEGORIA);
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - AMB Categorias</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container padding-not-left-right contenido-estilo" style="padding-bottom: 10px;">
				<table class="table">
				  <thead class="thead-light">
				    <tr>
				    	<th scope="col">Fila</th>
				      	<th scope="col">ID Categoría</th>
				      	<th scope="col">Nombre Categoría</th>
				      	<th scope="col">Acciones</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<%
				  	int contar = 1;
				  	for(Categoria categoria : listaCategoria) {%>
				  		<tr>
					      <th scope="row"><%=contar%></th>
					      <td><%=categoria.getIdCategoria()%></td>
					      <td><%=categoria.getDescripcion()%></td>
					      <td>
					      	<form action="<%=request.getContextPath()%><%=WebRequest.ABM_CATEGORIAS%>" method="post">
			      				<input type="hidden" name="accionABMCategoria" value="editar"/>
			      				<input type="hidden" name="accionABMCategoriaEdit" value="<%=categoria.getIdCategoria()%>"/>
			      				<button type="submit">Editar</button>
					      	</form>
					      	<form action="<%=request.getContextPath()%><%=WebRequest.ABM_CATEGORIAS%>" method="post">
					      		<input type="hidden" name="accionABMCategoria" value="eliminar"/>
			      				<input type="hidden" name="accionABMCategoriaEliminar" value="<%=categoria.getIdCategoria()%>"/>
			      				<button type="submit">Eliminar</button>
					      	</form>
					      </td>
				    	</tr>
				  	<%
				  		contar++;
				  	}%>
				  </tbody>
				</table>
				<div class="col-sm-12">
					<form action="<%=request.getContextPath()%><%=WebRequest.ABM_CATEGORIAS%>" method="post">
						<input type="hidden" name="accionABMCategoria" value="crear"/>
						<button type="submit">Crear Categoría</button>
					</form>
				</div>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>