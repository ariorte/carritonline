<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%@page import="py.com.carritonline.util.Utils"%>
<%@page import="py.com.carritonline.util.Constantes"%>
<%@page import="py.com.carritonline.bean.Usuario" %>
<%@page import="py.com.carritonline.bean.Item"%>
<%
Usuario usuario = (Usuario)request.getSession().getAttribute(Constantes.USUARIO);
List<Item> items = (List<Item>) request.getSession().getAttribute(Constantes.AGREGAR_CARRITO);
int cantidadCarrito = 0;
if(items != null){
	cantidadCarrito = items.size();
}
%>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.INICIO%>">Inicio</a>
</div>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.PRODUCTOS%>">Productos</a>
</div>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.CONFIRMACION%>">Carrito (<%=cantidadCarrito%>)</a>
</div>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="#" title="Usuario <%= usuario.getNombre()%>">
		<%=usuario.getNombre()%>
    </a>
</div>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<form id="form-logout" action="<%=request.getContextPath()%><%=WebRequest.INICIAR_SESION%>" method="post">
		<input type="hidden" name="accionLogin" value="logout"/>
        <a href="#" onclick="document.getElementById('form-logout').submit()" title="Cerra Sesi�n">
        	Cerrar Sesi�n
		</a>
	</form>
</div>


<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.ABM_PRODUCTOS%>">ABM Productos</a>
</div>
<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.ABM_CATEGORIAS%>">ABM Categor�as</a>
</div>
<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.ABM_USUARIOS%>">ABM Usuarios</a>
</div>

