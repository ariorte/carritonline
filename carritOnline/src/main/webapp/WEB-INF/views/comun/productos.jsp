<%@page import="java.util.List"%>
<%@page import="py.com.carritonline.util.Constantes"%>
<%@page import="py.com.carritonline.bean.Producto"%>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%
List<Producto> listaProductos = (List<Producto>)request.getSession().getAttribute(Constantes.LISTA_COMPLETO_PRODUCTO);
%>
<div class="container padding-not-left-right contenido-estilo">
	<table class="table" style="margin-bottom: 0px;">
		<thead class="thead-light">
			<tr>
				<th scope="col">Fila</th>
				<th scope="col">Descripci�n</th>
				<th scope="col">Imagen</th>
				<th scope="col">Precio x Unidad</th>
				<th scope="col">Acci�n</th>
			</tr>
		</thead>
		<tbody>
			<%
				int contar = 1;
				for (Producto producto : listaProductos) {
			%>
			<tr>
				<th scope="row"><%=contar%></th>
				<td><%=producto.getDescripcion()%></td>
				<td><img style="background-image: url('<%=request.getContextPath()%>/resources/imgs/<%=producto.getNombreImg()%>.png'); width: 168px; height: 138px;"></td>
				<td><%=producto.getPrecioUnit()%></td>
				<td>
					<form
						action="<%=request.getContextPath()%><%=WebRequest.PRODUCTO_DETALLE%>"
						method="post">
						<input type="hidden" value="<%=producto.getIdProducto()%>"
						name="idProducto" />
						<button type="submit">Ver Detalles</button>
					</form>
				</td>
			</tr>
			<%
				contar++;
				}
			%>
		</tbody>
	</table>
</div>