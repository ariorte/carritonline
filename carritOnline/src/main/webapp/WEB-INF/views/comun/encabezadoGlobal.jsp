<%@page import="java.util.List"%>
<%@page import="py.com.carritonline.util.Constantes"%>
<%@page import="py.com.carritonline.util.Utils"%>
<%@page import="py.com.carritonline.bean.Producto"%>
<%@page import="py.com.carritonline.bean.Usuario" %>
<%
Usuario usuario = (Usuario)request.getSession().getAttribute(Constantes.USUARIO);
int tipoUser = (usuario != null ? usuario.getTipoUsuario() : -1);
%>
<%if(usuario == null){%>
	<jsp:include page="/WEB-INF/views/comun/encabezado.jsp"></jsp:include>
<%} else if (usuario != null && tipoUser == 0 ){%>
	<jsp:include page="/WEB-INF/views/comun/encabezadoAdmin.jsp"></jsp:include>
<%} else if (usuario != null && tipoUser == 1){%>
	<jsp:include page="/WEB-INF/views/comun/encabezadoUser.jsp"></jsp:include>
<%}%>