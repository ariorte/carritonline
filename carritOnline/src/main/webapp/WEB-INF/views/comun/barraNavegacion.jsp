<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%@page import="py.com.carritonline.util.Utils"%>
<%@page import="py.com.carritonline.bean.Producto"%>
<%@page import="py.com.carritonline.util.Constantes"%>
<%@page import="py.com.carritonline.bean.Item"%>
<%
List<Producto> listaProductos = (List<Producto>)request.getSession().getAttribute(Constantes.LISTA_COMPLETO_PRODUCTO);
List<Item> items = (List<Item>) request.getSession().getAttribute(Constantes.AGREGAR_CARRITO);
int cantidadCarrito = 0;
if(items != null){
	cantidadCarrito = items.size();
}
%>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.INICIO%>">Inicio</a>
</div>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.PRODUCTOS%>">Productos</a>
</div>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.CONFIRMACION%>">Carrito (<%=cantidadCarrito%>)</a>
</div>

<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
	<a href="<%= request.getContextPath()%><%=WebRequest.INICIAR_SESION%>">Iniciar Sesi�n</a>
</div>