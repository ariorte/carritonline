<%@page import="py.com.carritonline.common.request.WebRequest" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String mensajeError = (String) request.getAttribute("NO_EXISTE");
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - Iniciar Sesión</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container contenido-estilo" style="padding-bottom: 10px;">
				<form action="<%=request.getContextPath()%><%=WebRequest.INICIAR_SESION%>" method="post" autocomplete="off">
					<div class="row" style="text-align: left;">
						<div class="col-sm-3">
							<label>Usuario*:</label>
						</div>
						<div class="col-sm-3">
							<input name="username" type="text" title="Ingrese el usuario" placeholder="Ingrese el usuario" required="required" />
						</div>
					</div>
					<div class="row" style="text-align: left;">
						<div class="col-sm-3">
							<label>Contraseña*:</label>
						</div>
						<div class="col-sm-3">
							<input name="password" type="password" title="Ingrese la constraseña" placeholder="Ingrese la contraseña" required="required" />
						</div>
					</div>
					<div class="col-sm-12">
						<button type="submit">Login</button>
					</div>
					<input type="hidden" name="accionLogin" value="login" />
				</form>
				<%if("mostrarErrorLogin".equals(mensajeError)){%>
					<div class="col-sm-12" id="mensajeError">
						<label>Usuario no válido</label>
					</div>					
				<%} %>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>