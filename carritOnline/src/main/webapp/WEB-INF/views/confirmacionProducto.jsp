<%@page import="java.util.List" %>
<%@page import="py.com.carritonline.util.Constantes" %>
<%@page import="py.com.carritonline.bean.Item" %>
<%@page import="py.com.carritonline.bean.Usuario" %>
<%@page import="py.com.carritonline.bean.Producto" %>
<%@page import="py.com.carritonline.common.request.WebRequest"%>
<%@page import="py.com.carritonline.dao.CarritonlineDbDaoImplement" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
List<Item> items = (List<Item>)request.getSession().getAttribute(Constantes.AGREGAR_CARRITO);
List<Producto> productos = (List<Producto>)request.getSession().getAttribute(Constantes.CONFIRMACION_CARRITO_PRODUCTO);
Usuario usuario = (Usuario)request.getSession().getAttribute(Constantes.USUARIO);
if(usuario == null){
	usuario = new Usuario();
	usuario.setIdUsuario(-1);
}
Producto productoAux = null;
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IT Services - Confirmacion Producto</title>
		<jsp:include page="/WEB-INF/views/comun/comun-css.jsp"/>
		<jsp:include page="/WEB-INF/views/comun/comun-js.jsp"/>
		<script type="text/javascript">
			function validarUsuarioIngresado() {
				let idUsuario = parseInt(document.getElementById("idUsuario").value);
				if (idUsuario < 1) {
					alert("Debe iniciar sesion para confirmar la compra");
					return false;
				}
				return true;
			}
		</script>
	</head>
	<body>
		<div id="main">
			<jsp:include page="/WEB-INF/views/comun/encabezadoGlobal.jsp"></jsp:include>
			<div class="container padding-not-left-right contenido-estilo" style="padding-bottom: 10px;">
				<%if(items == null || items.size() == 0) {%>
					<div class="row">
						<label>NO EXISTE PRODUCTO POR CONFIRMAR</label>
					</div>
				<%} else if(items.size() > 0){%>
					<table class="table">
					<thead class="thead-light">
						<tr>
							<th scope="col">Fila</th>
							<th scope="col">Descripción</th>
							<th scope="col">Imagen</th>
							<th scope="col">Precio x Unidad</th>
							<th scope="col">Cantidad</th>
							<th scope="col">Total (Precio x Cantidad)</th>
							<th scope="col">Acción</th>
						</tr>
					</thead>
					<tbody>
						<%
							int contar = 1;
							for (Item item : items) {
								for(Producto producto : productos){
									if(producto.getIdProducto().equals(item.getIdProducto())){
										productoAux = producto;
									}
								}
						%>
						<tr>
							<th scope="row"><%=contar%></th>
							<td><%=productoAux.getDescripcion()%></td>
							<td><img style="background-image: url('<%=request.getContextPath()%>/resources/imgs/<%=productoAux.getNombreImg()%>.png'); width: 168px; height: 138px;"></td>
							<td><%=productoAux.getPrecioUnit()%></td>
							<th><%=item.getCantidad()%></th>
							<td><%=item.getPrecio()%></td>
							<td>
								<form action="<%=request.getContextPath()%><%=WebRequest.PRODUCTOS%>" method="post">
									<input type="hidden" value="eliminarProductoCarrito" name="accion" />
									<input type="hidden" value="<%=productoAux.getIdProducto()%>" name="idProducto" />
									<button type="submit">Eliminar Producto</button>
								</form>
							</td>
						</tr>
						<%
							contar++;
							}
						%>
					</tbody>
				</table>
				<%}%>
				<div>
					<form action="<%=request.getContextPath()%><%=WebRequest.PRODUCTOS%>">
						<input type="hidden" value="confirmarCompraProductoCarrito" name="accion" />
						<input type="hidden" value="<%=usuario.getIdUsuario()%>" id="idUsuario" name="idUsuario" />
						<button type="submit" onclick="return validarUsuarioIngresado()">
							Confirmar Compra
						</button>
					</form>
				</div>
			</div>
			<jsp:include page="/WEB-INF/views/comun/pie.jsp"></jsp:include>
		</div>
	</body>
</html>