package py.com.carritonline.util;

public class Constantes {
	// Parametros API
	public static final String LISTA_COMPLETO_PRODUCTO = "lista.completo.producto";
	public static final String USUARIO = "usuario";
	public static final String PRODUCTO = "producto";
	public static final String LISTA_COMPLETO_CATEGORIA = "lista.completo.categoria";
	public static final String CATEGORIA = "categoria";
	public static final String LISTA_COMPLETO_USUARIO = "lista.completo.usuario";
	public static final String AGREGAR_CARRITO = "agregar.carrito";
	public static final String CONFIRMACION_CARRITO_PRODUCTO = "confirmacion.producto.carrito";
}
