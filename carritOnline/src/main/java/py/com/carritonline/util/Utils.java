package py.com.carritonline.util;

public class Utils {
	private static final String VERSION = "1.0.0";
	
	public static boolean validarDatosProductoACrear(String idProducto, 
			String descripcion, String cantidad, String precio, String imgs,
			String idCategoria) {
		if (esNuloCampo(idProducto) || esNuloCampo(descripcion) || esNuloCampo(cantidad) || esNuloCampo(precio)
				|| esNuloCampo(imgs) || esNuloCampo(idCategoria) || esVacioCadena(idCategoria)
				|| esVacioCadena(idProducto) || esVacioCadena(descripcion) || esVacioCadena(cantidad)
				|| esVacioCadena(precio) || esVacioCadena(imgs)) {
			return true;
		}
		return false;
	}
	
	private static boolean esNuloCampo(Object object) {
		if (object == null) {
			return true;
		}
		return false;
	}

	private static boolean esVacioCadena(String campo) {
		if ("".equals(campo.trim())) {
			return true;
		}
		return false;
	}
	
	public static boolean validarDatosCategoria(String idCategoria,
			String descripcion) {
		if (esNuloCampo(idCategoria) || esNuloCampo(descripcion)
				|| esVacioCadena(idCategoria) || esVacioCadena(descripcion)) {
			return true;
		}
		return false;
	}
	
	public static boolean validarDatosUsuario(String nombre, String apellido, 
			String logiName, String contrasena, String tipoUser ) {
		if (esNuloCampo(nombre) || esNuloCampo(apellido) || esNuloCampo(logiName)
				|| esNuloCampo(contrasena) || esNuloCampo(tipoUser)
				|| esVacioCadena(nombre) || esVacioCadena(apellido)
				|| esVacioCadena(logiName) || esVacioCadena(contrasena)
				|| esVacioCadena(tipoUser)) {
			return true;
		}
		return false;
	}
	
	/**
	 * @return the version
	 */
	public static String getVersion() {
		return VERSION;
	}
	
}
