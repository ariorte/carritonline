package py.com.carritonline.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import py.com.carritonline.bean.Categoria;
import py.com.carritonline.bean.Item;
import py.com.carritonline.bean.Producto;
import py.com.carritonline.bean.TransaccionCabecera;
import py.com.carritonline.bean.TransaccionDetalle;
import py.com.carritonline.bean.Usuario;
import py.com.carritonline.services.CarritonlineService;
import py.com.carritonline.util.Constantes;
import py.com.carritonline.util.Md5Java;
import py.com.carritonline.util.Utils;

@Controller
public class WebController {
	
	@Autowired
	protected CarritonlineService carritoNlineService;

	@RequestMapping(value="/index", method = {RequestMethod.GET, RequestMethod.POST})
	public String index() {
		return "/index";
	}
	
	@RequestMapping(value = "/inicio", method = {RequestMethod.GET})
	public String inicio(HttpServletRequest request) {
		
		System.out.println("Dentro del WebController de inicio");
		
		List<Producto> listaProductos = carritoNlineService.getProductos();
		
		System.out.println("Por enviar a la vista la lista de productos: " 
				+ listaProductos == null ? "vacio" : listaProductos.size());
		
		Usuario usuario = (Usuario) request.getSession().getAttribute(Constantes.USUARIO);
		
		request.getSession().setAttribute(Constantes.USUARIO, usuario);
		request.getSession().setAttribute(Constantes.LISTA_COMPLETO_PRODUCTO, listaProductos);
		
		return "/inicio";
	}
	
	@SuppressWarnings({ "unchecked", "null" })
	@RequestMapping(value = "/productos", method = {RequestMethod.GET, RequestMethod.POST})
	public String productos(HttpServletRequest request) {
		System.out.println("Dentro del controlador de productos");
		
		String accion = request.getParameter("accion");
		List<Item> items = null;
		String idProducto = null;
		
		if ("anhadir".equals(accion)) {
			System.out.println("Se debe anhadir al carrito y desminuiar la "
					+ "cantida del producto");
			items = (List<Item>) request.getSession().getAttribute(Constantes.AGREGAR_CARRITO);
			
			if (items == null) {
				items = new ArrayList<Item> ();
			}
			int cantidad = Integer.parseInt(request.getParameter("cantidad"));
			idProducto = (String) request.getParameter("idProducto");
			Producto producto = carritoNlineService.getProducto(idProducto);
			boolean existeProductoCarrito = false;

			if (items.size() > 0) {
				for (Item item : items) {
					if (idProducto.equals(item.getIdProducto())) {
						item.setPrecio(new BigDecimal(producto.getPrecioUnit() * cantidad));
						item.setCantidad(cantidad);
						existeProductoCarrito = true;
					}
				}
				if (!existeProductoCarrito) {
					items.add(new Item(producto.getIdProducto(), 
							new BigDecimal(producto.getPrecioUnit() * cantidad), 
							cantidad));
				}
			} else if (items.size() == 0) {
				items.add(new Item(producto.getIdProducto(), 
						new BigDecimal(producto.getPrecioUnit() * cantidad), 
						cantidad));
			}
			carritoNlineService.modificarCantidadProducto(producto.getIdProducto(), 
					cantidad, false);
			request.getSession().setAttribute(Constantes.AGREGAR_CARRITO, items);
		} else if("eliminarProductoCarrito".equals(accion)) {
			idProducto = (String) request.getParameter("idProducto");
			items = (List<Item>) request.getSession().getAttribute(Constantes.AGREGAR_CARRITO);
			int cantidad = 0;
			
			int posicionRemoverProducto = -1;
			int contador = 0;
			
			for (Item item : items) {
				if (idProducto.equals(item.getIdProducto())) {
					posicionRemoverProducto = contador;
					cantidad = item.getCantidad();
				} else {
					contador++;
				}
			}
			if (posicionRemoverProducto > -1) {
				carritoNlineService.modificarCantidadProducto(idProducto, 
						cantidad, true);
				items.remove(posicionRemoverProducto);
			}
			request.getSession().setAttribute(Constantes.AGREGAR_CARRITO, items);
		} else if("confirmarCompraProductoCarrito".equals(accion)) {
			items = (List<Item>) request.getSession().getAttribute(Constantes.AGREGAR_CARRITO);
			String idUsuario = (String) request.getParameter("idUsuario");
			String envio = "Asunción, Paraguay";
			byte idMediopago = 1;
			int nroTarjeta = 12345678;
			String estado = "I";
			Date date = new Date();
			DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
			String fechaHora = hourdateFormat.format(date);
			List<TransaccionDetalle> listaTransaccionesDetalles = new ArrayList<TransaccionDetalle>();
			BigDecimal totalCompra = new BigDecimal(0);
			int contarItem = 0;
			int precioProducto = 0;
			
			for (Item item : items) {
				contarItem++;
				totalCompra = totalCompra.add(item.getPrecio());
				precioProducto = carritoNlineService.getProducto(item.getIdProducto()).getPrecioUnit();
				listaTransaccionesDetalles.add(new TransaccionDetalle(contarItem, 
						item.getIdProducto(), item.getCantidad(), item.getPrecio(), precioProducto));
			}
			
			
			TransaccionCabecera cabecera = new TransaccionCabecera(fechaHora, 
					Integer.valueOf(idUsuario), totalCompra, envio, idMediopago,
					nroTarjeta, estado, listaTransaccionesDetalles);
			
			carritoNlineService.confirmarCompraProductos(cabecera);
			
			request.getSession().setAttribute(Constantes.AGREGAR_CARRITO, null);
			request.getSession().setAttribute(Constantes.CONFIRMACION_CARRITO_PRODUCTO, null);
		}
		System.out.println("Por recuperar la lista completa de productos");
		
		List<Producto> listaProductos = carritoNlineService.getProductos();
		
		System.out.println("Por enviar a la vista la lista de productos: " 
				+ listaProductos == null ? "vacio" : listaProductos.size());

		request.getSession().setAttribute(Constantes.LISTA_COMPLETO_PRODUCTO, listaProductos);
		
		return "/productos";
	}
	
	@RequestMapping(value = "/iniciarSesion", method = {RequestMethod.GET, RequestMethod.POST})
	public String iniciarSesion(HttpServletRequest request) {
		
		System.out.println("Dentro del controlador de iniciar sesion");
		
		String accion = request.getParameter("accionLogin");
		
		accion = (accion == null ? "" : accion.trim());
		
		System.out.println("Se tiene que realizar la siguiente accion: "
				+ (accion == null ? "ir a inicio": accion));
		
		Usuario usuario = null;
		
		switch (accion) {
		case "login":
			System.out.println("Por evaluar si son correctos los datos");
			String username = request.getParameter("username");
			String password = Md5Java.md5Java(request.getParameter("password"));
			usuario = carritoNlineService.comprobarLoginUsuario(username, password);
			request.getSession().setAttribute(Constantes.USUARIO, usuario);
			if (usuario == null) {
				request.setAttribute("NO_EXISTE", "mostrarErrorLogin");
				return "/iniciarSesion";
			}
			return "/inicio";
		case "logout":
			System.out.println("Por cerrar sesión del usuario");
			request.getSession().setAttribute(Constantes.USUARIO, null);
			return "/inicio";
		default:
			return "/iniciarSesion";
		}
	}
	
	@RequestMapping(value = "/abmProductos", method = {RequestMethod.GET, 
			RequestMethod.POST})
	public String abmProductos(HttpServletRequest request) {
		
		System.out.println("Dentro del WebController de abmProductos");
		
		String accion = request.getParameter("accionAMBProducto");
		
		accion = (accion == null ? "" : accion.trim());
		
		List<Producto> listaProductos = null;
		String idProducto = null;
		Producto producto = null;
		
		switch (accion) {
		case "crear":
			return "/admin/productoCrear";
		case "eliminar":
			idProducto = request.getParameter("accionAMBProductoEliminar");
			
			boolean eliminado = carritoNlineService.eliminarProducto(idProducto);
			
			request.setAttribute("eliminado", eliminado);
			
			listaProductos = carritoNlineService.getProductos();
			System.out.println("Por enviar a la vista la lista de productos: " 
					+ listaProductos == null ? "vacio" : listaProductos.size());
			request.getSession().setAttribute(Constantes.LISTA_COMPLETO_PRODUCTO, listaProductos);
			return "/admin/abmProductos";
		case "editar":
			idProducto = request.getParameter("accionAMBProductoEdit");
			producto = carritoNlineService.getProducto(idProducto);
			request.getSession().setAttribute(Constantes.PRODUCTO, producto);
			return "/admin/productoEditar";
		default:
			listaProductos = carritoNlineService.obtenerListCompletoProductos();
			System.out.println("Por enviar a la vista la lista de productos: " 
					+ listaProductos == null ? "vacio" : listaProductos.size());
			request.getSession().setAttribute(Constantes.LISTA_COMPLETO_PRODUCTO, listaProductos);
			return "/admin/abmProductos";
		}
		
	}
	
	@RequestMapping(value = "/abmCategorias", method = {RequestMethod.GET, 
			RequestMethod.POST})
	public String ambCategorias(HttpServletRequest request) {
		System.out.println("Dentro del WebController de abmCategorias");
		
		String accion = request.getParameter("accionABMCategoria");
		
		accion = (accion == null ? "" : accion.trim());
		
		List<Categoria> categorias = null;
		String idCategoria = null;
		Categoria categoria = null;
		
		switch (accion) {
		case "crear":
			return "/admin/categoria/categoriaCrear";
		case "eliminar":
			idCategoria = request.getParameter("accionABMCategoriaEliminar");
			
			boolean eliminado = carritoNlineService.eliminarCategoria(idCategoria);
			
			request.setAttribute("eliminado", eliminado);
			
			categorias = carritoNlineService.obtenerListaCategoria();
			System.out.println("Por enviar a la vista la lista de categorias: " 
					+ categorias == null ? "vacio" : categorias.size());
			request.getSession().setAttribute(Constantes.LISTA_COMPLETO_CATEGORIA, categorias);
			return "/admin/abmCategorias";
		case "editar":
			idCategoria = request.getParameter("accionABMCategoriaEdit");
			categoria = carritoNlineService.obtenerCategoria(idCategoria);
			request.getSession().setAttribute(Constantes.CATEGORIA, categoria);
			return "/admin/categoria/categoriaEditar";
		default:
			categorias = carritoNlineService.obtenerListaCategoria();
			System.out.println("Por enviar a la vista la lista de categorias: " 
					+ categorias == null ? "vacio" : categorias.size());
			request.getSession().setAttribute(Constantes.LISTA_COMPLETO_CATEGORIA, categorias);
			return "/admin/abmCategorias";
		}
	}
	
	@RequestMapping(value = "/abmUsuarios", method = {RequestMethod.GET, 
			RequestMethod.POST})
	public String abmUsuarios(HttpServletRequest request) {
		System.out.println("Dentro del WebController de abmUsuarios");
		
		String accion = request.getParameter("accionABMUsuario");
		
		accion = (accion == null ? "" : accion.trim());
		
		List<Usuario> usuarios = null;
		String idUsuario = null;
		Usuario usuario = null;
		
		switch (accion) {
		case "crear":
			return "/admin/usuario/usuarioCrear";
		case "eliminar":
			idUsuario = request.getParameter("accionABMUsuarioEliminar");
			
			boolean eliminado = carritoNlineService.eliminarUsuario(idUsuario);
			
			request.setAttribute("eliminado", eliminado);
			
			usuarios = carritoNlineService.obtenerListaUsuarios();
			System.out.println("Por enviar a la vista la lista de usuarios: " 
					+ usuarios == null ? "vacio" : usuarios.size());
			request.getSession().setAttribute(Constantes.LISTA_COMPLETO_USUARIO, usuarios);
			return "/admin/abmUsuarios";
		case "editar":
			idUsuario = request.getParameter("accionABMUsuarioEdit");
			
			idUsuario = idUsuario == null ? "0" : idUsuario; 
			
			usuario = carritoNlineService.buscarUsuario(idUsuario);
			
			if (usuario != null) {
				usuario.setIdUsuario(Integer.parseInt(idUsuario));
			}
			
			request.getSession().setAttribute(Constantes.USUARIO, usuario);
			return "/admin/usuario/usuarioEditar";
		default:
			usuarios = carritoNlineService.obtenerListaUsuarios();
			System.out.println("Por enviar a la vista la lista de usuarios: " 
					+ usuarios == null ? "vacio" : usuarios.size());
			request.getSession().setAttribute(Constantes.LISTA_COMPLETO_USUARIO, usuarios);
			return "/admin/abmUsuarios";
		}
	}
	
	@RequestMapping(value = "/producto/detalle", method = {RequestMethod.POST})
	public String productoDetalles(HttpServletRequest request) {
		System.out.println("Dentro del controller para buscar el producto y "
				+ "mostrar su detalle");
		
		String idProducto = request.getParameter("idProducto");
		
		System.out.println("Se recupera el siguiente id del producto a buscar: "
				+ "idProducto["+ idProducto +"]");
		
		Producto producto = carritoNlineService.getProducto(idProducto);
		
		request.getSession().setAttribute(Constantes.PRODUCTO, producto);
		
		return "/productoDetalles";
	}
	
	@RequestMapping(value = "/productoNuevoEditado", method = {RequestMethod.POST})
	public String productoNuevoEditado(HttpServletRequest request) {
		
		String accion = request.getParameter("productoCrearEditar");
		
		accion = (accion == null ? "" : accion.trim());
		
		String idProducto = request.getParameter("idProducto");
		String descripcion = request.getParameter("descripcion");
		String cantidad = request.getParameter("cantidad");
		String precio = request.getParameter("precio");
		String imgs = request.getParameter("imgsNombre");
		String idCategoria = request.getParameter("idCategoria");
		
		boolean registroCorrecto = 
				Utils.validarDatosProductoACrear(idProducto, descripcion,
						cantidad, precio, imgs, idCategoria);
		
		boolean registrado = false;
		
		if (!registroCorrecto) {
			Producto producto = new Producto(idProducto, 
					Integer.parseInt(cantidad), descripcion, 
					Integer.parseInt(precio), imgs, idCategoria);
			
			switch (accion) {
			case "crear":
				registrado = carritoNlineService.guardarProductoNuevo(producto);
				break;
			default:
				registrado = carritoNlineService.actualizarProducto(producto);
				break;
			}
		} else {
			System.out.println("Dato inválido recibido, no se genera "
					+ "el registro para el producto");
		}
		
		request.setAttribute("registrado", registrado);
		
		List<Producto> listaProductos = carritoNlineService.getProductos();
		System.out.println("Por enviar a la vista la lista de productos: " 
				+ listaProductos == null ? "vacio" : listaProductos.size());
		request.getSession().setAttribute(Constantes.LISTA_COMPLETO_PRODUCTO, 
				listaProductos);
		
		return "/admin/abmProductos";
	}
	
	@RequestMapping(value = "/categoriaNuevo", method = {RequestMethod.POST})
	public String categoriaNuevo(HttpServletRequest request) {
		System.out.println("Dentro del controlador de crear nueva "
				+ "categoria");
		
		String accion = request.getParameter("categoriaCrear");
		
		accion = (accion == null ? "" : accion.trim());
		
		String idCategoria = request.getParameter("idCategoria");
		String descripcion = request.getParameter("descripcion");
		
		boolean camposCorrectos = Utils.validarDatosCategoria(idCategoria, 
				descripcion);
		
		boolean registrado = false;
		
		if (!camposCorrectos) {
			Categoria categoria = new Categoria(idCategoria, descripcion);
			registrado = carritoNlineService.crearCategoria(categoria);
		} else {
			System.out.println("Dato inválido recibido, no se genera "
					+ "el registro para la categoria");
		}
		
		request.setAttribute("registrado", registrado);
		
		List<Categoria> categorias = carritoNlineService.obtenerListaCategoria();
		System.out.println("Por enviar a la vista la lista de categorias: " 
				+ categorias == null ? "vacio" : categorias.size());
		request.getSession().setAttribute(Constantes.LISTA_COMPLETO_PRODUCTO, 
				categorias);
		
		return "/admin/abmCategorias";
	}
	
	
	@RequestMapping(value = "/categoriaEditado", method = {RequestMethod.POST})
	public String categoriaEditado(HttpServletRequest request) {
		System.out.println("Dentro del controlador que edita la "
				+ "categoria");
		
		String accion = request.getParameter("categoriaEditar");
		
		accion = (accion == null ? "" : accion.trim());
		
		String idCategoria = request.getParameter("idCategoria");
		String descripcion = request.getParameter("descripcion");
		
		boolean camposCorrectos = Utils.validarDatosCategoria(idCategoria, 
				descripcion);
		
		boolean registrado = false;
		
		if (!camposCorrectos) {
			Categoria categoria = new Categoria(idCategoria, descripcion);
			registrado = carritoNlineService.actualizacionCategoria(categoria);
		} else {
			System.out.println("Dato inválido recibido, no se genera "
					+ "el registro para la categoria");
		}
		
		request.setAttribute("registrado", registrado);
		
		List<Categoria> categorias = carritoNlineService.obtenerListaCategoria();
		System.out.println("Por enviar a la vista la lista de categorias: " 
				+ categorias == null ? "vacio" : categorias.size());
		request.getSession().setAttribute(Constantes.LISTA_COMPLETO_PRODUCTO, 
				categorias);
		
		return "/admin/abmCategorias";
	}
	
	@RequestMapping(value = "/usuarioNuevo", method = {RequestMethod.POST})
	public String crearUsuario(HttpServletRequest request) {
		System.out.println("Dentro del controlador para crear usuario");
		
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String logiName = request.getParameter("logiName");
		String contrasena = request.getParameter("contrasena");
		String tipoUser = request.getParameter("tipoUser");
		
		boolean camposCorrectos = Utils.validarDatosUsuario(nombre, apellido, 
				logiName, contrasena, tipoUser);
		
		boolean registrado = false;
		if (!camposCorrectos) {
			Usuario usuario = new Usuario(nombre, apellido, logiName, 
					Md5Java.md5Java(contrasena), Byte.valueOf(tipoUser));
			registrado = carritoNlineService.crearUsuario(usuario);
		} else {
			System.out.println("Dato invalido no se genera el registro del "
					+ "usuario");
		}
		
		request.setAttribute("registrado", registrado);
		
		List<Usuario> usuarios = carritoNlineService.obtenerListaUsuarios();
		System.out.println("Por enviar a la vista la lista de usuarios: " 
				+ usuarios == null ? "vacio" : usuarios.size());
		request.getSession().setAttribute(Constantes.LISTA_COMPLETO_USUARIO, 
				usuarios);
		
		return "/admin/abmUsuarios";
	}
	
	@RequestMapping(value = "/usuarioEditado", method = {RequestMethod.POST})
	public String editarUsuario(HttpServletRequest request) {
		System.out.println("Dentro del controlador para editar usuario");
		
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String logiName = request.getParameter("logiName");
		String contrasena = request.getParameter("contrasena");
		String tipoUser = request.getParameter("tipoUser");
		
		boolean camposCorrectos = Utils.validarDatosUsuario(nombre, apellido, 
				logiName, contrasena, tipoUser);
		
		boolean registrado = false;
		if (!camposCorrectos) {
			Usuario usuario = new Usuario(Integer.parseInt(request.getParameter("idUsuario")), nombre, apellido, logiName, 
					Md5Java.md5Java(contrasena), Byte.valueOf(tipoUser));
			registrado = carritoNlineService.editarUsuario(usuario);
		} else {
			System.out.println("Dato invalido no se edita al usuario");
		}
		
		request.setAttribute("registrado", registrado);
		
		List<Usuario> usuarios = carritoNlineService.obtenerListaUsuarios();
		System.out.println("Por enviar a la vista la lista de usuarios: " 
				+ usuarios == null ? "vacio" : usuarios.size());
		request.getSession().setAttribute(Constantes.LISTA_COMPLETO_USUARIO, 
				usuarios);
		
		return "/admin/abmUsuarios";
	}
	
	@RequestMapping(value = "/confirmacion", method = {RequestMethod.GET, RequestMethod.POST})
	public String confirmarProducto(HttpServletRequest request) {
		System.out.println("Dentro del controller para agregar al carrito el "
				+ "producto");
		@SuppressWarnings("unchecked")
		List<Item> items = (List<Item>) request.getSession().getAttribute(Constantes.AGREGAR_CARRITO);
		
		if (items == null || items.size() == 0) {
			request.getSession().setAttribute(Constantes.CONFIRMACION_CARRITO_PRODUCTO, null);
		} else if (items.size() > 0) {
			List<Producto> productos = new ArrayList<Producto>();
			
			for (Item item : items) {
				productos.add(carritoNlineService.getProducto(item.getIdProducto()));
			}
			request.getSession().setAttribute(Constantes.CONFIRMACION_CARRITO_PRODUCTO, productos);	
		}
		
		return "/confirmacionProducto";
	}
	
}
