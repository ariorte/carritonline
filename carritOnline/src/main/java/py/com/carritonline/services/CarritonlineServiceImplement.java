package py.com.carritonline.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.carritonline.bean.Categoria;
import py.com.carritonline.bean.Producto;
import py.com.carritonline.bean.TransaccionCabecera;
import py.com.carritonline.bean.Usuario;
import py.com.carritonline.dao.CarritonlineDbDao;

@Service
public class CarritonlineServiceImplement implements CarritonlineService {
	
	private CarritonlineDbDao carritoNlineDbDao;
	
	@Autowired
	public void setCarritoNlineDbDao(CarritonlineDbDao carritoNlineDbDao) {
		this.carritoNlineDbDao = carritoNlineDbDao;
	}

	@Override
	public List<Producto> getProductos() {
		System.out.println("Desde servicio para el lado Dao de recuperar "
				+ "todos los productos" );
		return carritoNlineDbDao.obtenerProductos();
	}

	@Override
	public Producto getProducto(String idProducto) {
		System.out.println("Desde el servicio para obtener el producto por el "
				+ "id llamando al Dao");
		return carritoNlineDbDao.obtenerProducto(idProducto);
	}

	@Override
	public Usuario comprobarLoginUsuario(String username, String password) {
		System.out.println("Desde el servicio para comprobar los datos "
				+ "del usuario");
		return carritoNlineDbDao.comprobarUsuarioLogin(username, password);
	}

	@Override
	public boolean guardarProductoNuevo(Producto producto) {
		System.out.println("Desde el servicio para guardarProductoNuevo");
		return carritoNlineDbDao.crearProductoNuevo(producto);
	}

	@Override
	public boolean actualizarProducto(Producto producto) {
		System.out.println("Desde el servicio para actualizarProducto");
		return carritoNlineDbDao.actualizarProducto(producto);
	}

	@Override
	public boolean eliminarProducto(String idProducto) {
		System.out.println("Desde el servicio para eliminarProducto");
		return carritoNlineDbDao.eliminarProducto(idProducto);
	}

	@Override
	public List<Categoria> obtenerListaCategoria() {
		System.out.println("Desde el servicio para obtener la lista "
				+ "completa de las categorias");
		return carritoNlineDbDao.obtenerCategorias();
	}

	@Override
	public boolean crearCategoria(Categoria categoria) {
		System.out.println("Dentro del servicio para crear la categoria");
		return carritoNlineDbDao.crearCategoria(categoria);
	}

	@Override
	public Categoria obtenerCategoria(String idCategoria) {
		System.out.println("Dentro del servicio para recuperar la categoria "
				+ "por el id");
		return carritoNlineDbDao.obtenerCategoria(idCategoria);
	}

	@Override
	public boolean actualizacionCategoria(Categoria categoria) {
		System.out.println("Dentro del servicio para actualizar la categoria");
		return carritoNlineDbDao.actualizarCategoria(categoria);
	}

	@Override
	public boolean eliminarCategoria(String idCategoria) {
		System.out.println("Dentro del servicio para pasar el id de la "
				+ "categoria por ser eliminada");
		return carritoNlineDbDao.eliminarCategoria(idCategoria);
	}

	@Override
	public List<Usuario> obtenerListaUsuarios() {
		System.out.println("Dentro del servicio para recuperar la lista "
				+ "de usuarios");
		return carritoNlineDbDao.listaCompletaUsuarios();
	}

	@Override
	public boolean crearUsuario(Usuario usuario) {
		System.out.println("Dentro del servicio para recuperar crear el "
				+ "usuario");
		return carritoNlineDbDao.crearUsuarioNuevo(usuario);
	}

	@Override
	public Usuario buscarUsuario(String idUsuario) {
		System.out.println("Dentro del servicio para buscar el registro "
				+ "del usuario");
		return carritoNlineDbDao.obtenerUsuario(idUsuario);
	}

	@Override
	public boolean editarUsuario(Usuario usuario) {
		System.out.println("Dentro del servicio de editar usuario");
		return carritoNlineDbDao.editarUsuario(usuario);
	}

	@Override
	public boolean eliminarUsuario(String idUsuario) {
		System.out.println("Dentro del Dao para eliminar el usuario");
		return carritoNlineDbDao.eliminarUsuario(idUsuario);
	}

	@Override
	public List<Producto> obtenerListCompletoProductos() {
		System.out.println("Dentro del servicio para obtener la lista completa "
				+ "de los productos");
		return carritoNlineDbDao.obtenerProductosCompleto();
	}

	@Override
	public void modificarCantidadProducto(String idProducto, int cantidad, boolean aumentar) {
		System.out.println("Dentro del servicio para modificar la cantidad del producto");
		carritoNlineDbDao.modificarCantidadProducto(idProducto, cantidad, aumentar);
	}

	@Override
	public void confirmarCompraProductos(TransaccionCabecera cabecera) {
		System.out.println("Dentro del servicio que confirma la compra de "
				+ "los productos");
		carritoNlineDbDao.confirmacionCompraProductos(cabecera);
	}

}
