package py.com.carritonline.services;

import java.util.List;

import py.com.carritonline.bean.Categoria;
import py.com.carritonline.bean.Producto;
import py.com.carritonline.bean.TransaccionCabecera;
import py.com.carritonline.bean.Usuario;

public interface CarritonlineService {
	
	/**
	 * El servicios va a realizar el llamado al Dao para recuperar la lista de productos
	 * @return Una lista de productos si es lo encuentra en caso contrario null
	 */
	public List<Producto> getProductos();
	
	/**
	 * El servicio espera recibir el dato del id del producto para ser buscado
	 * @param idProducto representa el dato único del producto a ser buscado
	 * @return el producto como objeto en el caso que se encuentra por su id
	 * en caso contrario retorna null
	 */
	public Producto getProducto(String idProducto);
	
	/**
	 * Este servicio espera recibir los parametros correspondiente para 
	 * poder realizar la comprobacion de las credenciales del usuario
	 * @param username es del tipo String
	 * @param password es del tipo String
	 * @return el objecto Usuario en caso contrario null
	 */
	public Usuario comprobarLoginUsuario(String username, String password);
	
	/**
	 * El servicio va a estar esperando recibir un objecto producto a ser 
	 * registrado en la base de datos.
	 * @return un boolean siendo true que se genero el registro en caso 
	 * contrario false que hubo algun tipo error al tratar de generar el 
	 * registro.
	 */
	public boolean guardarProductoNuevo(Producto producto);
	
	/**
	 * El servicio va a estar esperando recibir un objeto producto para 
	 * ser actualizado los campos a nivel de base de datos.
	 * @param producto es del tipo Producto.
	 * @return el tipo de dato boolean en donde true es que pudo actualizar 
	 * en caso contrarior es false que ocurrio error alguno.
	 */
	public boolean actualizarProducto(Producto producto);

	/**
	 * Este servicio espera recibir el id del producto para poder ser 
	 * eliminado a nivel de base de datos.
	 * @param idProducto es del tipo String.
	 * @return el tipo de dato boolean en donde true es que fue eliminado 
	 * el registro en caso contrario hubo algun inconveniente para eliminar 
	 * el producto a nivel de base de datos.
	 */
	public boolean eliminarProducto(String idProducto);
	
	/**
	 * Este servicio va a estar respondiendo con la lista de categorias 
	 * en caso que exista en caso contrario una lista vacio (null)
	 * @return el tipo de dato List que representa la lista de Categorias
	 */
	public List<Categoria> obtenerListaCategoria();
	
	/**
	 * Este servicio va a estar viendo en pasar la categoria a ser creada
	 * @param categoria es del tipo Categoria
	 * @return el tipo de dato boolean en donde true es que fue creado 
	 * el registro en caso contrario false que hubo algun error
	 */
	public boolean crearCategoria(Categoria categoria);
	
	/**
	 * Este servicio va a facilitar el dato del id de la categoria a ser 
	 * buscado.
	 * @param idCategoria es del tipo String.
	 * @return un objecto del tipo Categoria.
	 */
	public Categoria obtenerCategoria(String idCategoria);
	
	/**
	 * Este servicio va a facilitar el objeto Categoria para que se pueda 
	 * actualizar.
	 * @param categoria es del tipo Categoria.
	 * @return el tipo de dato boolean siendo true que pudo actualziar en caso 
	 * contrario false.
	 */
	public boolean actualizacionCategoria(Categoria categoria);
	
	/**
	 * Este servicio va a facilitar el paso del id de la categoria para 
	 * que se pueda eliminar su registro.
	 * @param idCategoria es esperado que sea del tipo String.
	 * @return el tipo de dato boolean en donde true indica que fue eliminado 
	 * el registro en caso contrario false.
	 */
	public boolean eliminarCategoria(String idCategoria);
	
	/**
	 * Este servicio va a estar invoncando al Dao para recuperar la lista 
	 * de usuarios
	 * @return el tipo de dato List que representa a los Usuarios.
	 */
	public List<Usuario> obtenerListaUsuarios();
	
	/**
	 * Este servicio va a estar llamando al Dao para que se pueda crear 
	 * el nuevo usuario con los datos que se encuentra el objeto del
	 * tipo Usuario.
	 * @param usuario se espera que sea del tipo Usuario.
	 * @return el tipo de boolean en donde true es que fue creado en caso 
	 * contrario false.
	 */
	public boolean crearUsuario(Usuario usuario);
	
	/**
	 * Este servicio va a estar llamando al Dao para poder buscar el 
	 * registro del usuario por medio de su id.
	 * @param idUsuario es esperado que sea del tipo String.
	 * @return un objeto del tipo Usuario.
	 */
	public Usuario buscarUsuario(String idUsuario);
	
	/***
	 * Este servicio va a actualizar los datos del usuario llamando al 
	 * dao correspondiente.
	 * @param usuario es del tipo Usuario lo esperado.
	 * @return el tipo de dato boolean siendo true que fue editado en caso 
	 * contrario false.
	 */
	public boolean editarUsuario(Usuario usuario);
	
	/**
	 * Este servicio va a estar pasando el id del usuario a ser eliminado.
	 * @param idUsuario se espera del tipo String.
	 * @return un tipo de dato boolean en donde true es que fue eliminado 
	 * en caso contrario false.
	 */
	public boolean eliminarUsuario(String idUsuario);
	
	/**
	 * Este servicio recupera la lista completa de los productos
	 * @return el tipo de dato List que contiene objetos del tipo Producto
	 */
	public List<Producto> obtenerListCompletoProductos();
	
	/**
	 * Este servicio va facilitar los valores recibidos en los parametros 
	 * para realizar la operacion de modificacion de la cantidad
	 * @param idProducto es esperado del tipo String que representa el id del 
	 * producto.
	 * @param cantidad es esperado del tipo int que representa la cantidad a 
	 * ser modificado del producto.
	 * @param aumentar es esperado del tipo boolean que representa true si se 
	 * quiere aumentar en caso contrario disminuir la cantidad.
	 */
	public void modificarCantidadProducto(String idProducto, int cantidad, boolean aumentar);
	
	/**
	 * Este servicio va a facilitar el paso de la transaccion ya que se 
	 * confirma la compra.
	 * @param cabecera es del tipo esperado TransaccionCabecera.
 	 */
	public void confirmarCompraProductos(TransaccionCabecera cabecera);
}
