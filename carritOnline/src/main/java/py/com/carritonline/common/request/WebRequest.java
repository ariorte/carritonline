package py.com.carritonline.common.request;

public class WebRequest {
	public static final String INICIO = "/web/inicio";
	public static final String INICIAR_SESION = "/web/iniciarSesion";
	
	public static final String CONFIRMACION = "/web/confirmacion";
	
	public static final String ABM_PRODUCTOS = "/web/abmProductos";
	public static final String ABM_CATEGORIAS = "/web/abmCategorias";
	public static final String ABM_USUARIOS = "/web/abmUsuarios";
	
	public static final String PRODUCTO_DETALLE= "/web/producto/detalle";
	public static final String PRODUCTOS = "/web/productos";
	public static final String PRODUCTO_NUEVO_EDITADO = "/web/productoNuevoEditado";
	
	public static final String CATEGORIA_NUEVO = "/web/categoriaNuevo"; 
	public static final String CATEGORIA_EDITADO = "/web/categoriaEditado";
	
	public static final String USUARIO_NUEVO = "/web/usuarioNuevo";
	public static final String USUARIO_EDITADO = "/web/usuarioEditado";
	
	public static final String AGREGAR_CARRITO = "/web/agregarCarrito";
}
