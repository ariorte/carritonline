package py.com.carritonline.dao;

import java.util.List;

import py.com.carritonline.bean.Categoria;
import py.com.carritonline.bean.Producto;
import py.com.carritonline.bean.TransaccionCabecera;
import py.com.carritonline.bean.Usuario;

public interface CarritonlineDbDao {
	
	/**
	 * Se obtiene los registros de la base de datos que son los productos.
	 * @return una lista de los registros que son los productos.
	 */
	public List<Producto> obtenerProductos();
	
	/**
	 * Esta función va a recibir el id del producto para ser buscado en la 
	 * en la base de datos por la misma.
	 * @param idProducto es el id único del producto a ser buscado.
	 * @return el registro representado en el objeto de Producto o bien null 
	 * si no se encuentra algún registro en la base de datos.
	 */
	public Producto obtenerProducto(String idProducto);
	
	/**
	 * Esta función va a evaluar si las credenciales del usuario que esta 
	 * probando login son validas
	 * @param username es del tipo String
	 * @param password es del tipo String
	 * @return el objeto Usuario si es que existe en caso contrario NULL
	 */
	public Usuario comprobarUsuarioLogin(String username, String password);
	
	/**
	 * La función va a recibir un objeto producto para poder crear el registro 
	 * a nivel de base de datos
	 * @param producto es esperado que sea del Tipo Producto
	 * @return un boolean indicando true insercion correcta en caso contrario 
	 * false que no se pudo insertar
	 */
	public boolean crearProductoNuevo(Producto producto);
	
	/**
	 * Esta función va a recibir un objeto producto para poder actualizar 
	 * los campos que fueron modificados
	 * @param producto es del tipo Producto
	 */
	public boolean actualizarProducto(Producto producto);
	
	/**
	 * La función va a recibir el id del producto a ser eliminado su 
	 * registro de la base de datos/
	 * @param idProducto es del tipo String.
	 * @return el tipo de dato boolean siendo true que pudo eliminar el 
	 * registro en caso contrarior false que corresponde a un error al 
	 * tratar de eliminar.
	 */
	public boolean eliminarProducto(String idProducto);
	
	/***
	 * Se recupera todos los registros de la base de datos que serian 
	 * las categorias.
	 * @return el tipo de dato List que corresponde a Categoria.
	 */
	public List<Categoria> obtenerCategorias();

	/**
	 * Esta funcion va a registrar la nueva categoria
	 * @param categoria es del tipo Categoria
	 * @return el tipo de dato boolean siendo true que pudo crear el 
	 * registro en caso contario false que no se pudo crear
	 */
	public boolean crearCategoria(Categoria categoria);
	
	/**
	 * Esta funcion va a recuperar el registro de la categoria por medio 
	 * del id de la misma.
	 * @param idCategoria es del tipo String.
	 * @return el tipo de Categoria que seria la categoria encontrada.
	 */
	public Categoria obtenerCategoria(String idCategoria);
	
	/**
	 * Esta funcion va a actualizar el registro que corresponde a la 
	 * categoria los campos correspondientes
	 * @param categoria es del tipo Categoria
	 * @return el tipo de dato boolean en donde true que se pudo actualizar 
	 * en caso contrario false ya que hubo algun error
	 */
	public boolean actualizarCategoria(Categoria categoria);
	
	/**
	 * Esta funcion va a eliminar el registro de la categoria identificado 
	 * por su id
	 * @param idCategoria es del tipo String.
	 * @return el tipo de dato boolean en donde true es que fue eliminado en 
	 * caso contrario false.
	 */
	public boolean eliminarCategoria(String idCategoria);
	
	/**
	 * Esta función va a retornar una lista completa de los usuarios.
	 * @return un tipo de dato List que representa cada uno a los Usuarios.
	 */
	public List<Usuario> listaCompletaUsuarios();
	
	/**
	 * Esta función va a crear el registro correspondiente al nuevo 
	 * usuario.
	 * @param usuario es del tipo Usuario el esperado.
	 * @return un tipo de dato boolean siendo true que fue creado en caso 
	 * contrario false.
	 */
	public boolean crearUsuarioNuevo(Usuario usuario);
	
	/**
	 * Esta función va a buscar el registro del usuario.
	 * @param idUsuario es esperado del tipo String.
	 * @return un objeto del tipo Usuario.
	 */
	public Usuario obtenerUsuario(String idUsuario);
	
	/**
	 * Esta funcion va a editar los datos del usuario.
	 * @param usuario es esperado que sea del tipo Usuario.
	 * @return el tipo de dato boolean en donde true es que fue editado 
	 * en caso contrario false.
	 */
	public boolean editarUsuario(Usuario usuario);
	
	/**
	 * Esta funcion va a eliminar el registro del usuario de la base de datos
	 * @param idUsuario esperado del tipo String
	 * @return el tipo de dato boolean en donde true es que fue eliminado 
	 * en caso contrario false (hubo algun error/no fue eliminado).
	 */
	public boolean eliminarUsuario(String idUsuario);
	
	/**
	 * Esta funcion va a recuperar la lista completo de los productos 
	 * con la cantidad igual o mayor a cero.
	 * @return el tipo de Datos List que contiene cada objeto del tipo Producto
	 */
	public List<Producto> obtenerProductosCompleto();
	
	/**
	 * Este procedimiento va a estar modificando la cantidad del producto 
	 * pudiendo ser mayor o menor.
	 * @param idProducto se espera del tipo String que representa el id del 
	 * producto.
	 * @param cantidad se espera del tipo int que representa la cantidad a 
	 * ser aumentada o disminuida.
	 * @param aumentar se espera del tipo boolean que representa el true 
	 * que se va a aumentar la cantidad en el parametro cantidad en caso 
	 * contrario se va a estar disminuyendo.
	 */
	public void modificarCantidadProducto(String idProducto, int cantidad, 
			boolean aumentar);
	
	/**
	 * Este procedimiento va a registrar la cabecera como los detalles 
	 * de los compra de los productos.
	 * @param cabecera se espera del tipo TransaccionCabecera.
	 */
	public void confirmacionCompraProductos(TransaccionCabecera cabecera);
}
