package py.com.carritonline.dao;

import java.util.List;

import javax.sql.DataSource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import py.com.carritonline.bean.Categoria;
import py.com.carritonline.bean.Producto;
import py.com.carritonline.bean.TransaccionCabecera;
import py.com.carritonline.bean.TransaccionDetalle;
import py.com.carritonline.bean.Usuario;

@Repository
public class CarritonlineDbDaoImplement implements CarritonlineDbDao {

	private static final String listaProductoTodoQuery = "select id_producto, "
			+ "cantidad, descripcion, preciounit, nombre_img, id_categoria " 
			+ "from productos "
			+ "where cantidad > 0";
	private static final String productoDetalleQuery = "select id_producto, "
			+ "cantidad, descripcion, preciounit, nombre_img, id_categoria "
			+ "from productos "
			+ "where id_producto = ?";
	private static final String usuarioLoginQuery = "select id_usuario, "
			+ "nombre, apellido, tipo_usuario " 
			+ "from usuario " 
			+ "where upper(login_name) = upper(?) and contrasena = ?";
	private static final String usuarioLoginCountQuery = "select count(*) "
			+ "from usuario "
			+ "where upper(login_name) = upper(?) and contrasena = ?";
	private static final String productoInsertQuery = "insert into productos "
			+ "(id_producto, cantidad, descripcion, preciounit, nombre_img, id_categoria) "
			+ "values (?,?,?,?,?,?)";
	private static final String productoUpdateQuery = "update productos "
			+ "set cantidad = ?, descripcion = ?, preciounit = ?, nombre_img = ? "
			+ "where id_producto = ?";
	private static final String productoDeleteQuery = "delete from productos "
			+ "where id_producto = ?";
	private static final String listaCategoriaQuery = "select id_categoria, nombre_categoria\n" + 
			"from categorias";
	private static final String crearCategoriaQuery = "insert into "
			+ "categorias (id_categoria, nombre_categoria) "
			+ "values (?,?)";
	private static final String recuperarCategoriaQuery = "select id_categoria, nombre_categoria " 
			+ "from categorias " 
			+ "where id_categoria = ?";
	private static final String actualizarCategoriaQuery = "update categorias "
			+ "set nombre_categoria = ? "
			+ "where id_categoria = ?";
	private static final String eliminarCategoriaQuery = "delete from categorias "
			+ "where id_categoria = ?";
	private static final String listaUsuariosQuery = "select id_usuario, login_name, contrasena, apellido, nombre, tipo_usuario " 
			+ "from usuario";
	private static final String crearUsuarioQuery = "insert into "
			+ "usuario (login_name, contrasena, apellido, nombre, tipo_usuario) "
			+ "values (?,?,?,?,?)";
	private static final String recuperaUsuarioQuery = "select login_name, contrasena, apellido, nombre, tipo_usuario " 
			+ "from usuario " 
			+ "where id_usuario = ?";
	private static final String editarUsuarioQuery = "update usuario set "
			+ "login_name = ?, contrasena = ?, apellido = ?, nombre = ?, tipo_usuario = ? "
			+ "where id_usuario = ?";
	private static final String eliminarUsuarioQuery = "delete from usuario "
			+ "where id_usuario = ?";
	private static final String listaProductoCompletoQuery = "select id_producto, "
			+ "cantidad, descripcion, preciounit, nombre_img, id_categoria " 
			+ "from productos";
	private static final String productoCantidadAumentarQuery = "update productos "
			+ "set cantidad = (cantidad + ?) "
			+ "where id_producto = ?";
	private static final String productoCantidadDisminuirQuery = "update productos "
			+ "set cantidad = (cantidad - ?) "
			+ "where id_producto = ?";
	private static final String confirmarProductoQuery = "insert into "
			+ "transaccioncab (id_usuario, fecha, total, direccion_envio, estado) "
			+ "values (?,?,?,?,?)";
	private static final String recuperarMaxIdTransaccionCabeceraSql= "select max(id_transaccion) maxCabecera " 
			+"from transaccioncab";
	private static final String confirmarDetallesProductoQuery = "insert into "
			+ "transacciondet (id_transaccion, id_producto, cantidad, precio, subtotal) "
			+ "values (?,?,?,?,?)";
	
	protected JdbcTemplate jdbc;
	
	@Autowired
	public void setJdbc(DataSource dataSource) {
		this.jdbc = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Producto> obtenerProductos() {
		List<Producto> productoList = null;
		
		try {
			System.out.println("Se va recuperar la lista de productos");
			productoList = jdbc.query(listaProductoTodoQuery, productoListMapper);
		} catch (Exception e) {
			System.out.println("Se tuvo siguiente error al buscar todos los "
					+ "productos: >>>>>" + e.getMessage() +"<<<<<");
		} finally {
			System.out.println("Culminado la ejecucion del query");
		}
		System.out.println("Se va a retornar la siguiente cantidad de "
				+ "productos " + (productoList == null ? 0 : productoList.size()));
		
		return productoList;
	}

	@Override
	public Producto obtenerProducto(String idProducto) {
		Producto producto = null;
		System.out.println("Se va a estar ejecutando el siguiente query");
		System.out.println(">>>>>"+productoDetalleQuery+"<<<<<");
		
		try {
			producto = (Producto) jdbc.queryForObject(productoDetalleQuery, new Object[] {idProducto}, productoDetalleMapper);
		} catch (Exception e) {
			System.out.println("Se tuvo siguiente error al buscar el producto "
					+ ">>>>>"+e.getMessage()+"<<<<<");
		} finally {
			System.out.println("Finalizado ejecucion de query");
		}
		
		System.out.println("Se a recuperado la cantidad de registro: "
				+ (producto == null ? "cero (retorna null)" : "uno"));
		return producto;
	}
	
	@Override
	public Usuario comprobarUsuarioLogin(String username, String password) {
		System.out.println("Dentro del Dao de comprobar si el usuario es "
				+ "valido su login");
		Usuario usuario = null;
		int cantidad = 0;
		try {
			
			cantidad = jdbc.queryForObject(usuarioLoginCountQuery, 
					new Object[] {username, password}, Integer.class);
			
			if (cantidad == 1) {
				usuario = (Usuario) jdbc.queryForObject(usuarioLoginQuery,
						new Object[] {username, password}, usuarioLoginMapper);
			} else if(cantidad > 1) {
				System.out.println("Usuarios multiples como los mismos datos");
			} else if (cantidad == 0) {
				System.out.println("No se tiene coincidencia");
			}
		} catch (Exception e) {
			System.out.println("Se tuvo el siguiente ERROR al comprobar el "
					+ "login del usuario >>>>>"+e.getMessage()+"<<<<<");
		} finally {
			System.out.println("Finalizacion de la ejecucion del query "
					+ "de COMPROBACION LOGIN");
		}
		System.out.println("Se recupero la cantidad de registro: "
				+ (usuario == null ? "cero (retorna null)" : "uno") );
		return usuario;
	}
	
	@Override
	public boolean crearProductoNuevo(Producto producto) {
		System.out.println("Dentro del Dao para crear el registro del "
				+ "producto");
		try {
			System.out.println("Por ejecutar el siguiente >>>>>"
					+ productoInsertQuery +"<<<<<");
			System.out.println("1:" + producto.getIdProducto());
			System.out.println("2:" + producto.getCantidad());
			System.out.println("3:" + producto.getDescripcion());
			System.out.println("4:" + producto.getPrecioUnit());
			System.out.println("5:" + producto.getNombreImg());
			System.out.println("6:" + producto.getIdCategoria());
			
			jdbc.update(productoInsertQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setString(1, producto.getIdProducto());
					preparedStatement.setInt(2, producto.getCantidad());
					preparedStatement.setString(3, producto.getDescripcion());
					preparedStatement.setInt(4, producto.getPrecioUnit());
					preparedStatement.setString(5, producto.getNombreImg());
					preparedStatement.setString(6, producto.getIdCategoria());
				}
			});
		} catch (Exception e) {
			System.out.println("Se tuvo el siguiente ERROR al tratar de "
					+ "insertar el nuevo producto>>>>>" + e.getMessage() 
					+ "<<<<<");
			return false;
		} finally {
			System.out.println("Terminado la ejecucion del insercion del "
					+ "producto");
		}
		return true;
	}
	
	@Override
	public boolean actualizarProducto(Producto producto) {
		System.out.println("Dentro del Dao para actualizar el registro del "
				+ "producto");
		try {
			System.out.println("Por ejecutar el siguiente >>>>>"
					+ productoUpdateQuery +"<<<<<");
			System.out.println("1:" + producto.getCantidad());
			System.out.println("2:" + producto.getDescripcion());
			System.out.println("3:" + producto.getPrecioUnit());
			System.out.println("4:" + producto.getNombreImg());
			System.out.println("5:" + producto.getIdProducto());
			
			jdbc.update(productoUpdateQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setInt(1, producto.getCantidad());
					preparedStatement.setString(2, producto.getDescripcion());
					preparedStatement.setInt(3, producto.getPrecioUnit());
					preparedStatement.setString(4, producto.getNombreImg());
					preparedStatement.setString(5, producto.getIdProducto());
				}
			});
		} catch (Exception e) {
			System.out.println("Se tuvo el siguiente ERROR al tratar de "
					+ "actualizar el producto>>>>>" + e.getMessage() 
					+ "<<<<<");
			return false;
		} finally {
			System.out.println("Terminado la ejecucion de la actualizacion del "
					+ "producto");
		}
		return true;
	}
	
	@Override
	public boolean eliminarProducto(String idProducto) {
		System.out.println("Dentro del Dao para eliminar el registro");
		
		try {
			System.out.println("Por ejecutar el siguiente query >>>>>" 
					+ productoDeleteQuery + "<<<<<");
			System.out.println("idProducto 1:" + idProducto);
			
			jdbc.update(productoDeleteQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setString(1, idProducto);
				}
			});
		} catch (Exception e) {
			System.out.println("Se tuvo el siguiente ERROR al tratar de "
					+ "eliminar el producto >>>>"+ e.getMessage() +"<<<<<");
			return false;
		} finally {
			System.out.println("Terminado la ejecucion para eliminar el "
					+ "registro del producto en la base de datos");
		}
		return true;
	}
	
	@Override
	public List<Categoria> obtenerCategorias() {
		System.out.println("Dentro del Dao para recupera la lista "
				+ "de categorias");
		List<Categoria> categorias = null;
		
		try {
			System.out.println("Por ejecutar el siguiente query >>>>>" 
							+ listaCategoriaQuery + "<<<<<");
			categorias = jdbc.query(listaCategoriaQuery, categoriaListMapper);
			
		} catch (Exception e) {
			System.out.println("Se tuvo ERROR al trata de recuperar la "
					+ "lista de categorias >>>>>" + e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Finalizado la ejecucion para recupera la "
					+ "lista de categorias");
		}
		System.out.println("Se recupera la cantidad de categorias: " 
						+ (categorias == null ? 0 : categorias.size()));
		return categorias;
	}
	
	@Override
	public boolean crearCategoria(Categoria categoria) {
		System.out.println("Dentro del Dao para crear la categoria");
		boolean creado = false;
		
		try {
			System.out.println("Por ejecutar el siguiente query >>>>>" 
							+ crearCategoriaQuery + "<<<<<");
			jdbc.update(crearCategoriaQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setString(1, categoria.getIdCategoria());
					preparedStatement.setString(2, categoria.getDescripcion());
				}
			});
			creado = true;
		} catch (Exception e) {
			System.out.println("Hubo un ERROR al tratar de crear la categoria "
					+ ">>>>>" + e.getMessage() + "<<<<<");
			creado = false;
		} finally {
			System.out.println("Finalizado la ejecucion del Dao para crear "
					+ "la categoria");
		}
		return creado;
	}
	
	@Override
	public Categoria obtenerCategoria(String idCategoria) {
		System.out.println("Dentro del Dao para buscar la categoria por "
				+ "medio del id");
		Categoria categoria = null;
		
		try {
			categoria = jdbc.queryForObject(recuperarCategoriaQuery, 
					new Object [] {idCategoria}, categoriaDetalleMapper);
		} catch (Exception e) {
			System.out.println("Hubo un error al tratar de buscar la categoria "
					+ " por su id >>>>>" + e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Terminado la ejecucion del Dao para recuperar "
					+ "la categoria por su id");
		}
		return categoria;
	}
	
	@Override
	public boolean actualizarCategoria(Categoria categoria) {
		System.out.println("Dentro del Dao para actualizar la categoria");
		boolean actualizado = false;
		
		try {
			System.out.println("Por ejecutar el siguiente query >>>>>" 
							+ actualizarCategoriaQuery + "<<<<<");
			System.out.println("1:" + categoria.getDescripcion());
			System.out.println("2:" + categoria.getIdCategoria());
			jdbc.update(actualizarCategoriaQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setString(1, categoria.getDescripcion());
					preparedStatement.setString(2, categoria.getIdCategoria());
				}
			});
			actualizado = true;
		} catch (Exception e) {
			System.out.println("Hubo ERROR al tratar de actualizar la "
					+ "categoria >>>>>" + e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Se finalizo el Dao que actualiza la categoria");
		}
		return actualizado;
	}
	
	@Override
	public boolean eliminarCategoria(String idCategoria) {
		System.out.println("Dentro de Dao para eliminar el registro de la "
				+ "categoria por medio de la identificacion de su id");
		boolean eliminado = false;
		try {
			System.out.println("Por ejecutar el query >>>>>" 
					+ eliminarCategoriaQuery + "<<<<<");
			System.out.println("1:idCategoria" + idCategoria);
			jdbc.update(eliminarCategoriaQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setString(1, idCategoria);
				}
			});
			eliminado = true;
		} catch (Exception e) {
			System.out.println();
		} finally {
			System.out.println("Se termina la ejecucion del Dao para eliminar "
					+ "la categoria");
		}
		return eliminado;
	}
	
	@Override
	public List<Usuario> listaCompletaUsuarios() {
		System.out.println("Dentro del Dao para recuperar la lista de "
				+ "usuarios");
		List<Usuario> usuarios = null;
		try {
			System.out.println("Por ejecutar el siguiente query >>>>>" 
							+ listaUsuariosQuery + "<<<<<");
			usuarios = jdbc.query(listaUsuariosQuery, usuarioListMapper);
		} catch (Exception e) {
			System.out.println("Hubo ERRROR al tratar de recuperar la "
					+ "lista completa de usuarios");
		} finally {
			System.out.println("Terminado la ejecucaion del query para "
					+ "recuperar la lista de usuarios");
		}
		System.out.println("Se recupero la siguiente cantidad de registros: "
				+ (usuarios == null ? "cero" : usuarios.size()));
		return usuarios;
	}
	
	@Override
	public boolean crearUsuarioNuevo(Usuario usuario) {
		System.out.println("Dentro del Dao que va a crear el usuario");
		boolean creado = false;
		try {
			System.out.println("Por ejecutar el siguiente query >>>>>" 
							+ crearUsuarioQuery + "<<<<<");
			System.out.println("1:" + usuario.getLoginName());
			System.out.println("2:" + usuario.getContrasenha());
			System.out.println("3:" + usuario.getApellido());
			System.out.println("4:" + usuario.getNombre());
			System.out.println("5:" + usuario.getTipoUsuario());
			
			jdbc.update(crearUsuarioQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setString(1, usuario.getLoginName());
					preparedStatement.setString(2, usuario.getContrasenha());
					preparedStatement.setString(3, usuario.getApellido());
					preparedStatement.setString(4, usuario.getNombre());
					preparedStatement.setByte(5, usuario.getTipoUsuario());
				}
			});
			creado = true;
		} catch (Exception e) {
			System.out.println("ERROR al tratar de crear el usuario >>>>>"
							+ e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Culminado ejecucion de la funcion "
					+ "que crea un nuevo usuario");
		}
		System.out.println("Por retornar [" + creado + "]");
		return creado;
	}
	

	@Override
	public Usuario obtenerUsuario(String idUsuario) {
		System.out.println("Dentro del Dao para buscar el registros del "
				+ "usuario por medio del idUsuario");
		Usuario usuario = null;
		try {
			System.out.println("Por ejeuctar el siguiente query >>>>>" 
							+ recuperaUsuarioQuery + "<<<<<");
			System.out.println("1:idUsuario " + idUsuario);
			usuario = jdbc.queryForObject(recuperaUsuarioQuery, 
					new Object [] {Integer.valueOf(idUsuario)}, usuarioDetalleMapper);
		} catch (Exception e) {
			System.out.println("ERROR al buscar el usuario >>>>>" 
							+ e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Terminado la ejecucuion de la funcion "
					+ "para buscar el usuario");
		}
		return usuario;
	}
	

	@Override
	public boolean editarUsuario(Usuario usuario) {
		System.out.println("Dentro del Dao para editar datos del usuario");
		boolean actualizado = false;
		try {
			System.out.println("Se va a ejecutar el siguente query >>>>>" 
							+ editarUsuarioQuery + "<<<<<");
			System.out.println("1:login_name" + usuario.getLoginName());
			System.out.println("2:contrasena" + usuario.getContrasenha());
			System.out.println("3:apellido" + usuario.getApellido());
			System.out.println("4:nombre" + usuario.getNombre());
			System.out.println("5:tipo_usuario" + usuario.getTipoUsuario());
			System.out.println("6:idUsuario" + usuario.getIdUsuario());
			
			jdbc.update(editarUsuarioQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setString(1, usuario.getLoginName());
					preparedStatement.setString(2, usuario.getContrasenha());
					preparedStatement.setString(3, usuario.getApellido());
					preparedStatement.setString(4, usuario.getNombre());
					preparedStatement.setByte(5, usuario.getTipoUsuario());
					preparedStatement.setInt(6, usuario.getIdUsuario());
				}
			});
			actualizado = true;
		} catch (Exception e) {
			System.out.println("ERROR al tratar de editar el usuario >>>>>" 
							+ e.getMessage()  + "<<<<<");
		} finally {
			System.out.println("Se termina la ejecucion del Dao para editar "
					+ "dato del usuario");
		}
		return actualizado;
	}
	
	@Override
	public boolean eliminarUsuario(String idUsuario) {
		System.out.println("Dentro del Dao para eliminar le registro "
				+ "del usuario");
		boolean eliminacion = false;
		try {
			System.out.println("Por ejecutar el siguiente query >>>>>" 
							+ eliminarUsuarioQuery +"<<<<<");
			System.out.println("1:" + idUsuario);
			jdbc.update(eliminarUsuarioQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setInt(1, Integer.parseInt(idUsuario));
				}
			});
			eliminacion = true;
		} catch (Exception e) {
			System.out.println("ERROR al tratar de eliminar le registro "
					+ "del usuario >>>> " + e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Finalizado el Dao de eliminacion del registro "
					+ "del usuario");
		}
		return eliminacion;
	}
	
	@Override
	public List<Producto> obtenerProductosCompleto() {
		System.out.println("Dentro del Dao para recuperar la lista completa de "
				+ "productos");
		List<Producto> productos = null;
		try {
			System.out.println("Por ejecutar el siguiente query >>>>>" 
							+ listaProductoCompletoQuery + "<<<<<");
			productos= jdbc.query(listaProductoCompletoQuery, productoListMapper);
		} catch (Exception e) {
			System.out.println("ERROR al ejecutar el query >>>>>" 
					+ e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Terminado la ejecucion del Dao que recupera "
					+ "la lista de productos completo");
		}
		return productos;
	}

	@Override
	public void modificarCantidadProducto(String idProducto, int cantidad, boolean aumentar) {
		System.out.println("Dentro del procedimiento para cambiar la "
				+ "cantidad del producto");
		String sql = aumentar ? productoCantidadAumentarQuery : productoCantidadDisminuirQuery;
		try {
			System.out.println("Por ejecutar le siguiente query >>>>>" 
					+ sql + "<<<<<");
			System.out.println("1:cantidad " + cantidad);
			System.out.println("2:idProducto " + idProducto);
			jdbc.update(sql, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setInt(1, cantidad);
					preparedStatement.setString(2, idProducto);
				}
			});
		} catch (Exception e) {
			System.out.println("ERROR al tratar de modificar la "
					+ "cantidad del producto >>>>>" + e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Finalizacion del procedimiento para cambiar la "
					+ "cantidad del producto");
		}
	}
	
	@Override
	public void confirmacionCompraProductos(TransaccionCabecera cabecera) {
		System.out.println("Dentro del dao para registrar la compra de "
				+ "los productos");
		try {
			jdbc.update(confirmarProductoQuery, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement preparedStatement) throws SQLException {
					preparedStatement.setInt(1, cabecera.getIdUsuario());
					preparedStatement.setString(2, cabecera.getFecha());
					preparedStatement.setBigDecimal(3, cabecera.getTotal());
					preparedStatement.setString(4, cabecera.getDireccionEnvio());
					preparedStatement.setString(5, cabecera.getEstado());
				}
			});
			Integer pkTransaccionCabecera = jdbc.queryForObject(recuperarMaxIdTransaccionCabeceraSql, Integer.class);
			
			for (TransaccionDetalle transaccionDetalle : cabecera.getListaTransaccionesDetalles()) {
				jdbc.update(confirmarDetallesProductoQuery, new PreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement preparedStatement) throws SQLException {
						preparedStatement.setInt(1, pkTransaccionCabecera);
						preparedStatement.setString(2, transaccionDetalle.getIdProducto());
						preparedStatement.setInt(3, transaccionDetalle.getCantidad());
						preparedStatement.setBigDecimal(4, transaccionDetalle.getPrecio());
						preparedStatement.setInt(5, transaccionDetalle.getSubTotal());
					}
				});
			}
		} catch (Exception e) {
			System.out.println("ERROR al tratar de registrar >>>>>" 
					+ e.getMessage() + "<<<<<");
		} finally {
			System.out.println("Finalizacion de la ejecucion de confirmacion "
					+ "de compra de los productos");
		}
	}
	
	private static final RowMapper<Producto> productoListMapper = new RowMapper<Producto>() {
		public Producto mapRow(ResultSet resultSet, int rowNum) throws SQLException{
			return new Producto(resultSet.getString("id_producto"),
					resultSet.getInt("cantidad"), resultSet.getString("descripcion"),
					resultSet.getInt("preciounit"), resultSet.getString("nombre_img"),
					resultSet.getString("id_categoria"));
		}
	};
	
	private static final RowMapper<Producto> productoDetalleMapper = new RowMapper<Producto>() {
		public Producto mapRow(ResultSet resultSet, int rowNum) throws SQLException{
			return new Producto(resultSet.getString("id_producto"),
					resultSet.getInt("cantidad"), resultSet.getString("descripcion"),
					resultSet.getInt("preciounit"), resultSet.getString("nombre_img"),
					resultSet.getString("id_categoria"));
		}
	};
	
	private static final RowMapper<Usuario> usuarioLoginMapper = new RowMapper<Usuario>() {
		public Usuario mapRow(ResultSet resultSet, int rowNum) throws SQLException{
			return new Usuario(resultSet.getInt("id_usuario"), 
					resultSet.getString("nombre"), 
					resultSet.getString("apellido"), "", "",
					resultSet.getByte("tipo_usuario"));
		}
	};
	
	private static final RowMapper<Categoria> categoriaListMapper = new RowMapper<Categoria>() {
		public Categoria mapRow(ResultSet resultSet, int rowNum) throws SQLException{
			return new Categoria(resultSet.getString("id_categoria"), 
					resultSet.getString("nombre_categoria"));
		}
	};
	
	private static final RowMapper<Categoria> categoriaDetalleMapper = new RowMapper<Categoria>() {
		public Categoria mapRow(ResultSet resultSet, int rowNum) throws SQLException{
			return new Categoria(resultSet.getString("id_categoria"), 
					resultSet.getString("nombre_categoria"));
		}
	};

	private static final RowMapper<Usuario> usuarioListMapper = new RowMapper<Usuario>() {
		public Usuario mapRow(ResultSet resultSet, int rowNum) throws SQLException{
			return new Usuario(resultSet.getInt("id_usuario"), resultSet.getString("nombre"), 
					resultSet.getString("apellido"), resultSet.getString("login_name"), 
					resultSet.getString("contrasena"), resultSet.getByte("tipo_usuario"));
		}
	};

	private static final RowMapper<Usuario> usuarioDetalleMapper = new RowMapper<Usuario>() {
		public Usuario mapRow(ResultSet resultSet, int rowNum) throws SQLException{
			return new Usuario(resultSet.getString("nombre"), 
					resultSet.getString("apellido"), resultSet.getString("login_name"), 
					resultSet.getString("contrasena"), resultSet.getByte("tipo_usuario"));
		}
	};
	
}
