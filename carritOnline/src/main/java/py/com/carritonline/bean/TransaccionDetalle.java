package py.com.carritonline.bean;

import java.math.BigDecimal;

public class TransaccionDetalle {
	private int idTransaccion;
	private int item;
	private String idProducto;
	private int cantidad;
	private BigDecimal precio;
	private int subTotal;
	
	public TransaccionDetalle() {
	}

	public TransaccionDetalle(int idTransaccion, int item, String idProducto, int cantidad, BigDecimal precio,
			int subTotal) {
		this.idTransaccion = idTransaccion;
		this.item = item;
		this.idProducto = idProducto;
		this.cantidad = cantidad;
		this.precio = precio;
		this.subTotal = subTotal;
	}
	
	public TransaccionDetalle(int item, String idProducto, int cantidad, BigDecimal precio, int subTotal) {
		this.item = item;
		this.idProducto = idProducto;
		this.cantidad = cantidad;
		this.precio = precio;
		this.subTotal = subTotal;
	}

	/**
	 * @return the idTransaccion
	 */
	public int getIdTransaccion() {
		return idTransaccion;
	}

	/**
	 * @param idTransaccion the idTransaccion to set
	 */
	public void setIdTransaccion(int idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	/**
	 * @return the item
	 */
	public int getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(int item) {
		this.item = item;
	}

	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * @return the subTotal
	 */
	public int getSubTotal() {
		return subTotal;
	}

	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(int subTotal) {
		this.subTotal = subTotal;
	}

	@Override
	public String toString() {
		return "TransaccionDetalle [idTransaccion=" + idTransaccion + ", item=" + item + ", idProducto=" + idProducto
				+ ", cantidad=" + cantidad + ", precio=" + precio + ", subTotal=" + subTotal + "]";
	}
}
