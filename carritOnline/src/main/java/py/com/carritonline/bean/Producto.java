package py.com.carritonline.bean;

public class Producto {
	private String idProducto;
	private int cantidad;
	private String descripcion;
	private int precioUnit;
	private String nombreImg;
	private String idCategoria;
	
	public Producto() {
	}

	public Producto(String idProducto, int cantidad, String descripcion, int precioUnit, String nombreImg,
			String idCategoria) {
		this.idProducto = idProducto;
		this.cantidad = cantidad;
		this.descripcion = descripcion;
		this.precioUnit = precioUnit;
		this.nombreImg = nombreImg;
		this.idCategoria = idCategoria;
	}

	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the precioUnit
	 */
	public int getPrecioUnit() {
		return precioUnit;
	}

	/**
	 * @param precioUnit the precioUnit to set
	 */
	public void setPrecioUnit(int precioUnit) {
		this.precioUnit = precioUnit;
	}

	/**
	 * @return the nombreImg
	 */
	public String getNombreImg() {
		return nombreImg;
	}

	/**
	 * @param nombreImg the nombreImg to set
	 */
	public void setNombreImg(String nombreImg) {
		this.nombreImg = nombreImg;
	}

	/**
	 * @return the idCategoria
	 */
	public String getIdCategoria() {
		return idCategoria;
	}

	/**
	 * @param idCategoria the idCategoria to set
	 */
	public void setIdCategoria(String idCategoria) {
		this.idCategoria = idCategoria;
	}

	@Override
	public String toString() {
		return "Producto [idProducto=" + idProducto + ", cantidad=" + cantidad + ", descripcion=" + descripcion
				+ ", precioUnit=" + precioUnit + ", nombreImg=" + nombreImg + ", idCategoria=" + idCategoria + "]";
	}
}
