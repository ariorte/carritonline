package py.com.carritonline.bean;

import java.math.BigDecimal;

public class Item {
	private String idProducto;
	private BigDecimal precio;
	private int cantidad;
	
	
	public Item() {
	}


	public Item(String idProducto, BigDecimal precio, int cantidad) {
		this.idProducto = idProducto;
		this.precio = precio;
		this.cantidad = cantidad;
	}


	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}


	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}


	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}


	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}


	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}


	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	@Override
	public String toString() {
		return "Item [idProducto=" + idProducto + ", precio=" + precio + ", cantidad=" + cantidad + "]";
	}
}
