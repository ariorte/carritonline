package py.com.carritonline.bean;

import java.math.BigDecimal;
import java.util.List;

public class TransaccionCabecera {
	private int idTransaccion;
	private String fecha;
	private int idUsuario;
	private BigDecimal total;
	private String direccionEnvio;
	private byte idMedioPago;  // O Efectivo, 1 Tarjeta de Credito
	private int nroTarjeta;  // Solo si idMedioPago = 1
	private String estado;  // I ingresado
	private List<TransaccionDetalle> listaTransaccionesDetalles;
	
	public TransaccionCabecera() {
	}

	public TransaccionCabecera(int idTransaccion, String fecha, int idUsuario, BigDecimal total, String direccionEnvio,
			byte idMedioPago, int nroTarjeta, String estado, List<TransaccionDetalle> listaTransaccionesDetalles) {
		this.idTransaccion = idTransaccion;
		this.fecha = fecha;
		this.idUsuario = idUsuario;
		this.total = total;
		this.direccionEnvio = direccionEnvio;
		this.idMedioPago = idMedioPago;
		this.nroTarjeta = nroTarjeta;
		this.estado = estado;
		this.listaTransaccionesDetalles = listaTransaccionesDetalles;
	}

	public TransaccionCabecera(String fecha, int idUsuario, BigDecimal total, String direccionEnvio, byte idMedioPago,
			int nroTarjeta, String estado, List<TransaccionDetalle> listaTransaccionesDetalles) {
		this.fecha = fecha;
		this.idUsuario = idUsuario;
		this.total = total;
		this.direccionEnvio = direccionEnvio;
		this.idMedioPago = idMedioPago;
		this.nroTarjeta = nroTarjeta;
		this.estado = estado;
		this.listaTransaccionesDetalles = listaTransaccionesDetalles;
	}

	/**
	 * @return the idTransaccion
	 */
	public int getIdTransaccion() {
		return idTransaccion;
	}

	/**
	 * @param idTransaccion the idTransaccion to set
	 */
	public void setIdTransaccion(int idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the idUsuario
	 */
	public int getIdUsuario() {
		return idUsuario;
	}

	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	/**
	 * @return the direccionEnvio
	 */
	public String getDireccionEnvio() {
		return direccionEnvio;
	}

	/**
	 * @param direccionEnvio the direccionEnvio to set
	 */
	public void setDireccionEnvio(String direccionEnvio) {
		this.direccionEnvio = direccionEnvio;
	}

	/**
	 * @return the idMedioPago
	 */
	public byte getIdMedioPago() {
		return idMedioPago;
	}

	/**
	 * @param idMedioPago the idMedioPago to set
	 */
	public void setIdMedioPago(byte idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	/**
	 * @return the nroTarjeta
	 */
	public int getNroTarjeta() {
		return nroTarjeta;
	}

	/**
	 * @param nroTarjeta the nroTarjeta to set
	 */
	public void setNroTarjeta(int nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the listaTransaccionesDetalles
	 */
	public List<TransaccionDetalle> getListaTransaccionesDetalles() {
		return listaTransaccionesDetalles;
	}

	/**
	 * @param listaTransaccionesDetalles the listaTransaccionesDetalles to set
	 */
	public void setListaTransaccionesDetalles(List<TransaccionDetalle> listaTransaccionesDetalles) {
		this.listaTransaccionesDetalles = listaTransaccionesDetalles;
	}

	@Override
	public String toString() {
		return "TransaccionCabecera [idTransaccion=" + idTransaccion + ", fecha=" + fecha + ", idUsuario=" + idUsuario
				+ ", total=" + total + ", direccionEnvio=" + direccionEnvio + ", idMedioPago=" + idMedioPago
				+ ", nroTarjeta=" + nroTarjeta + ", estado=" + estado + ", listaTransaccionesDetalles="
				+ listaTransaccionesDetalles + "]";
	}
}
