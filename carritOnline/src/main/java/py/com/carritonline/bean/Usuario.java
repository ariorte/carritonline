package py.com.carritonline.bean;

public class Usuario {
	private int idUsuario;
	private String nombre;
	private String apellido;
	private String loginName;
	private String contrasenha;  // No en texto plano, cifrado en MD5
	private byte tipoUsuario;  // 0 Administrador, 1 Usuario normal
	
	public Usuario() {
	}

	public Usuario(int idUsuario, String nombre, String apellido, String loginName, String contrasenha,
			byte tipoUsuario) {
		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.apellido = apellido;
		this.loginName = loginName;
		this.contrasenha = contrasenha;
		this.tipoUsuario = tipoUsuario;
	}

	public Usuario(String nombre, String apellido, String loginName, String contrasenha, byte tipoUsuario) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.loginName = loginName;
		this.contrasenha = contrasenha;
		this.tipoUsuario = tipoUsuario;
	}

	/**
	 * @return the idUsuario
	 */
	public int getIdUsuario() {
		return idUsuario;
	}

	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return the contrasenha
	 */
	public String getContrasenha() {
		return contrasenha;
	}

	/**
	 * @param contrasenha the contrasenha to set
	 */
	public void setContrasenha(String contrasenha) {
		this.contrasenha = contrasenha;
	}

	/**
	 * @return the tipoUsuario
	 */
	public byte getTipoUsuario() {
		return tipoUsuario;
	}

	/**
	 * @param tipoUsuario the tipoUsuario to set
	 */
	public void setTipoUsuario(byte tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", nombre=" + nombre + ", apellido=" + apellido + ", loginName="
				+ loginName + ", contrasenha=" + contrasenha + ", tipoUsuario=" + tipoUsuario + "]";
	}
}
