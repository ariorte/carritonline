# Tienda Online
Es un proyecto Web Application en dónde se describe los siguientes puntos como guía para levantar el proyecto.

# Se debe tener instalar los siguiente:
 - El programa Spring Stool Suite en su versión 4, esto se puede consultar en el siguiente enlace: https://spring.io/tools
 - PgAdmin III.
 - Se debe tener instalado git.
 - Java 8.
 - Maven.
 - Cliente de postgres.
 - Instalar Dbeaver.
 
# Para poder preparar el ambiente y levantar el proyecto se debe realizar lo siguiente:
 - Clonar el repositorio: git clone https://gitlab.com/ariorte/carritonline.git
 - Una vez realizado la clonación se debe verificar que la rama actual sea master, esto se observa con el comando: git branch y en caso contrario que no se encuentre sobre la rama master para ubicar sobre la misma con el comando: git checkout master.
 - Se debe abrir el programa PgAdmin III y conectarse a la base de datos que viene con la configuración por defecto para luego realizar la restauración del backup de la base de datos del proyecto que se encuentro dentro del directorio BaseDeDatosBackup.
 - La configuración por defecto mencionado anteriormente del PgAdmin III sería con el usuario postgres y la contraseña postgres.
 - En caso contrario si se ingreso con otro usuario en el programa PgAdmin III se debe tener en cuenta que en el datasource del servidor a ser utilizado modificar user-name y password por el que corresponde.
 - Existe una carpeta denominada Servidor que contiene el wildfly-8.1.0.Final usado para el proyecto.
 - Con relación a la modificación del datasource eso se debe realizar dentro del directorio: wildfly-8.1.0.Final/standalone/configuration/ abriendo el archivo standalone.xml con el editor preferido y buscando la palabra "carritods" (sin las comillas dobles al momento de buscar) esto para ubicar de forma rápida en la sección de datasources el datasource que se usa para el proyecto.
 - Abrir el programa Spring Stool Suite, y realizando la importación del proyecto como: Existing Maven Projects
 - Una vez que termina la importación se debe tener en cuenta que el proyecto este siendo compilado con Java 8, en caso que no sea compilado con la versión mencionado anteriormente se debe cambiar a la misma.
 - Se debe realizar un Clean al Projects, esto se realiza en la barra de herramientas: Project -> Clean
 - Una vez culminado lo anterior se debe realizar click derecho sobre el proyecto importado para luego ir en la sección: Run As para realizar Maven Clean y Maven Install (se debe respetar la orden).
 - Se debe tener conexión a internet ya que contiene un pom.xml que descarga las despendencias usadas en el proyecto.
 - Agregar el servidor se realizada en la pestaña de Server y luego elegir WilFly 8.X e indicar el directorio en dónde se encuentra la misma (se encuentra dentro de la clonación).
 - Luego se debe realizar click derecho sobre le servidor y seleccionar Add and Remove para poder agregar el proyecto web.
 - Se debe ejecutar el servidor como Start o Start Debug.
 - Una vez culminado la carga se debe consultar la siguiente URL: http://localhost:8080/carritonline/web/inicio
 - El Dbeaver es para poder establecer conexión con la base de datos importado por medio de PgAdmin III y poder realizar por ejemplo: consultas.

# Los usuarios de pruebas que se encuentra en la base de datos son:
 - Usuario normal: login_name -> ariel y password -> 1234
 - Usuario Administrador: login_name -> admin y password -> 123456
